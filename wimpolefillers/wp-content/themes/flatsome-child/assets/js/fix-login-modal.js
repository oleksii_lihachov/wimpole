// When user press add to cart modal will not triggered
// So we manually re-run code snippet from /assets/flatsome.js

jQuery(document).ready(function () {
  jQuery(document).on("added_to_cart", function () {
    jQuery("[data-open]", document).on("click", function (t) {
      t.stopPropagation();
      var e = jQuery(t.currentTarget),
        i = e.data("open"),
        n = e.data("color"),
        o = e.data("bg"),
        r = e.data("pos"),
        s = e.data("visible-after"),
        a = e.data("class"),
        l = e.attr("data-focus");
      e.offset();
      e.addClass("current-lightbox-clicked"),
        jQuery.magnificPopup.open({
          
          items: {
            src: i,
            type: "inline",
            tLoading: '<div class="loading-spin dark"></div>',
          },
          removalDelay: 300,
          closeBtnInside: flatsomeVars.lightbox.close_btn_inside,
          closeMarkup: flatsomeVars.lightbox.close_markup,
          focus: l,
          callbacks: {
            beforeOpen: function () {
              this.st.mainClass = "off-canvas "
                .concat(n, " off-canvas-")
                .concat(r);
            },
            open: function () {
              jQuery("html").addClass("has-off-canvas"),
                jQuery("html").addClass("has-off-canvas-" + r),
                a && jQuery(".mfp-content").addClass(a),
                o && jQuery(".mfp-bg").addClass(o),
                jQuery(".mfp-content .resize-select").change(),
                jQuery.fn.packery &&
                  jQuery("[data-packery-options], .has-packery").packery(
                    "layout"
                  );
            },
            beforeClose: function () {
              jQuery("html").removeClass("has-off-canvas");
            },
            afterClose: function () {
              jQuery("html").removeClass("has-off-canvas-" + r),
                jQuery(".current-lightbox-clicked").removeClass(
                  "current-lightbox-clicked"
                ),
                s && jQuery(i).removeClass("mfp-hide");
            },
          },
        }),
        t.preventDefault();
    });
  });
});
