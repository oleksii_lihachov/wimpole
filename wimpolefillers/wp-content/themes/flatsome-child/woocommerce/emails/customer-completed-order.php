<?php

/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

if (!defined('ABSPATH')) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action('woocommerce_email_header', $email_heading, $email); ?>

<div style='display:block; text-align: center; margin-bottom: 32px;'><img style="margin: 0 auto; width:300px; height: auto;" src="<?php echo 'https://wimpolefillers.co.uk/wp-content/uploads/2023/03/WF_NS1-e1680183373271.png'; ?>" /></div>

<?php /* translators: %s: Customer first name */ ?>
<p><?php printf(esc_html__('Hi %s,', 'woocommerce'), esc_html($order->get_billing_first_name())); ?></p>
<p>Your order has been completed and dispatched.</p>
<p>You will receive an email soon with tracking details, Thank you for shopping with us at <span style="color: #ff6868;">Wimpole Fillers</span>, Looking forward to seeing you again.</p>

<?php
/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action('woocommerce_email_footer', $email);
