<?php

/**
 * Add custom Theme Functions here.
 *
 * @package null
 */

/**
 * Enqueue scripts and styles.
 */
function flatsome_child_scripts()
{
	// @codingStandardsIgnoreLine
	wp_enqueue_style(
		'flatsome_child_style',
		get_stylesheet_directory_uri() . '/dist/style.css',
		null,
		'1.0'
	);

	// @codingStandardsIgnoreLine
	wp_enqueue_script(
		'flatsome_child_script',
		get_stylesheet_directory_uri() . '/dist/bundle.js',
		array('jquery'),
		'1.0',
		true
	);
}
add_action('wp_enqueue_scripts', 'flatsome_child_scripts');



/**
 * Customer Reviews shortcode
 *
 * @param  mixed $atts attributes.
 * @return string
 */
function customer_reviews_func($atts)
{
	if (!is_array($atts)) {
		$atts = array();
	}

	// Repeater styles.
	$repater['id']                  = 'customer-reviews-' . rand();
	$repater['title']               = '';
	$repater['tag']                 = 'customer_reviews';
	$repater['class']               = 'c-customer-reviews';
	$repater['visibility']          = '';
	$repater['type']                = 'slider';
	$repater['style']               = 'default';
	$repater['slider_style']        = 'reveal';
	$repater['slider_nav_color']    = '';
	$repater['slider_nav_position'] = '';
	$repater['slider_bullets']      = true;
	$repater['auto_slide']          = '';
	$repater['row_spacing']         = 'small';
	$repater['row_width']           = 'full-width';
	$repater['columns']             = '5';
	$repater['columns__md']         = '';
	$repater['columns__sm']         = '';
	$repater['filter']              = '';
	$repater['depth']               = false;
	$repater['depth_hover']         = false;
	$args                           = array('post_type' => 'product');
	$comments                       = get_comments($args);

	if (empty($comments)) {
		return '';
	}

	ob_start();

	get_flatsome_repeater_start($repater);
	foreach ($comments as $comment) {
		$stars    = get_comment_meta($comment->comment_ID, 'rating', true);
		$star_row = '';

		$content = $comment->comment_content;
		$excerpt = $content;

		if (strlen($content) > 100) {
			$excerpt = strip_shortcodes($content);
			// @codingStandardsIgnoreLine 
			$excerpt = strip_tags($excerpt);
			$excerpt = substr($excerpt, 0, 100) . '...';
		}

		if ('1' === $stars) {
			$star_row = '<div class="star-rating"><span style="width:25%"><strong class="rating"></strong></span></div>';
		} elseif ('2' === $stars) {
			$star_row = '<div class="star-rating"><span style="width:35%"><strong class="rating"></strong></span></div>';
		} elseif ('3' === $stars) {
			$star_row = '<div class="star-rating"><span style="width:55%"><strong class="rating"></strong></span></div>';
		} elseif ('4' === $stars) {
			$star_row = '<div class="star-rating"><span style="width:75%"><strong class="rating"></strong></span></div>';
		} elseif ('5' === $stars) {
			$star_row = '<div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span></div>';
		}

?>
		<div class="col c-customer-reviews">
			<div class="col-inner">
				<div class="box box-text-bottom has-hover has-shadow">
					<div class="box-text">
						<div class="box-text-inner">
							<h5><?php echo esc_html($comment->comment_author); ?></h5>
							<?php
							// @codingStandardsIgnoreLine 
							echo $star_row;
							?>
							<div class="is-divider"></div>
							<div class="clearfix">
								<div class="c-customer-reviews__content">
									<p><?php echo esc_html($excerpt); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
	}

	get_flatsome_repeater_end($repater);

	$content = ob_get_contents();
	ob_end_clean();

	return $content;
}
add_shortcode('customer_reviews', 'customer_reviews_func');


if (!function_exists('flatsome_woocommerce_shop_loop_category')) {
	/**
	 * Add and/or Remove Categories
	 */
	function flatsome_woocommerce_shop_loop_category()
	{
		if (!flatsome_option('product_box_category')) {
			return;
		}
	?>
		<p class="category is-smaller no-text-overflow product-cat op-7">
			<?php
			global $product;
			$product_cats = function_exists('wc_get_product_category_list') ? wc_get_product_category_list(get_the_ID(), '\n', '', '') : $product->get_categories('\n', '', '');
			$product_cats = strip_tags($product_cats);

			if ($product_cats) {
				list($first_part) = explode('\n', $product_cats);
				echo esc_html(apply_filters('flatsome_woocommerce_shop_loop_category', $first_part, $product));
			}
			?>
		</p>
<?php
	}
}
add_action('woocommerce_shop_loop_item_title', 'flatsome_woocommerce_shop_loop_category', 0);

// Allow SVG
add_filter(
	'wp_check_filetype_and_ext',
	function ($data, $file, $filename, $mimes) {

		global $wp_version;
		if ($wp_version !== '4.7.1') {
			return $data;
		}

		$filetype = wp_check_filetype($filename, $mimes);

		return array(
			'ext'             => $filetype['ext'],
			'type'            => $filetype['type'],
			'proper_filename' => $data['proper_filename'],
		);
	},
	10,
	4
);

function cc_mime_types($mimes)
{
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function fix_svg()
{
	echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
             width: 100% !important;
             height: auto !important;
        }
        </style>';
}
add_action('admin_head', 'fix_svg');

add_filter(
	'woocommerce_registration_error_email_exists',
	function ($text, $email) {
		return __('An account is already registered with your email address. <a href="#" data-open="#login-form-popup" class="showlogin">Please log in.</a>', 'flatsome-child');
	},
	10,
	2
);

/**
 * @snippet       Deny Login Upon Registration @ WooCommerce My Account
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.7
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */

add_filter('woocommerce_registration_auth_new_customer', '__return_false');

//Disable XML-RPC 
// https://www.sonarsource.com/blog/wordpress-core-unauthenticated-blind-ssrf/
add_filter('xmlrpc_methods', function ($methods) {
	if (isset($methods['pingback.ping'])) {
		unset($methods['pingback.ping']);
	}

	if (isset($methods['pingback.extensions.getPingbacks'])) {
		unset($methods['pingback.extensions.getPingbacks']);
	}

	return $methods;
});
