<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'FORCE_SSL_ADMIN', true ); // Redirect All HTTP Page Requests to HTTPS - Security > Settings > Enforce SSL
// END iThemes Security - Do not modify or remove this line

//Begin Really Simple SSL session cookie settings
@ini_set('session.cookie_httponly', true);
@ini_set('session.cookie_secure', true);
@ini_set('session.use_only_cookies', true);
//END Really Simple SSL cookie settings

define( 'ITSEC_ENCRYPTION_KEY', 'Zz5aRH0mayNXVXJqfGVsMWI4T3w/ayZ5ckV0QS5xVHUzNXolZEE1bmFzTlBMTHkobmUreXhrc2RjUEhgOSNYfQ==' );

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dbs3093670');

/** MySQL database username */
define('DB_USER', 'dbu226829');

/** MySQL database password */
define('DB_PASSWORD', '7Pc7YDChEeCWGwvBaYZGSrK8fo6DQjyx');

/** MySQL hostname */
define('DB_HOST', 'db5003788670.hosting-data.io');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define( 'DBI_AWS_ACCESS_KEY_ID', '<DBI_AWS_ACCESS_KEY_ID>' );
define( 'DBI_AWS_SECRET_ACCESS_KEY', '<DBI_AWS_SECRET_ACCESS_KEY>' );

# Install pluging without ftp
define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '823f86d33b6349b8f34deb1d76bd118839547c3a');
define('SECURE_AUTH_KEY',  '21cb45749bcd5755a458cddf4bca69236c2f6af3');
define('LOGGED_IN_KEY',    'fc580376e8a1d91324a712d98f65802bd39e740b');
define('NONCE_KEY',        'f3f66b70686260e4332446db20973d9ee6b66322');
define('AUTH_SALT',        '27ae87e10f1749498b5538cab559e92c38b5e4fc');
define('SECURE_AUTH_SALT', 'f1ca9c10275aa7d511fca9a831deeee84ad57cf1');
define('LOGGED_IN_SALT',   '3215fa344955b79020d45047ccd4819556958b4a');
define('NONCE_SALT',       'b4b571888e07ff757f17638d8ad1a2012794d2ea');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

// block external requests except the white-list hosts
// define('WP_HTTP_BLOCK_EXTERNAL', true); 

/**
 * Handle SSL reverse proxy
 */
if ($_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
    $_SERVER['HTTPS']='on';
if (isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
    $_SERVER['HTTP_HOST'] = $_SERVER['HTTP_X_FORWARDED_HOST'];
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
