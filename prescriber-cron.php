<?php

require __DIR__ . '/wp-load.php';

$to = 'accounts@wimpolepharmacy.co.uk';

$headers[] = 'Content-Type: text/html; charset=UTF-8';
$headers[] = 'Bcc: oleksii.lihachov@gmail.com';

$subj = 'Prescriber expired notification';
$body = '';

$users = get_users([
    'role' => 'prescriber',
    'orderby' => 'user_nicename',
    'order' => 'ASC',
    'meta_key' => 'expired_at',
    'meta_value' => '',
    'meta_compare' => '!='
]);

$expired_users = [];

foreach ($users as $user) {
    $expiration_field = get_user_meta($user->ID, 'expired_at', true);

    if (empty($expiration_field)) {
        return;
    }

    $expired_timestamp = strtotime($expiration_field);

    $current_date_obj = new DateTime('today');
    $current_timestamp = $current_date_obj->getTimestamp();

    if ($current_timestamp > $expired_timestamp) {
        $expired_users[] = [
            'id' => $user->ID,
            'display_name' => $user->display_name,
            'user_email' => $user->user_email,
            'expired_at' => date('m/d/Y', $expired_timestamp),
        ];
    }
}

if (count($expired_users) > 0) {
    $body .= '<h3>Epired licenses:</h3>';
    $body .= '<table>';
    $body .= '  <tr>';
    $body .= '      <th style="padding: 10px;">ID</th>';
    $body .= '      <th style="padding: 10px;">Name</th>';
    $body .= '      <th style="padding: 10px;">Email</th>';
    $body .= '      <th style="padding: 10px;">Epired At</th>';
    $body .= '  </tr>';

    foreach ($expired_users as $user) {
        $body .= '  <tr>';
        $body .= '      <td style="padding: 10px;">' . $user['id'] . '</td>';
        $body .= '      <td style="padding: 10px;">' . $user['display_name'] . '</td>';
        $body .= '      <td style="padding: 10px;">' . $user['user_email'] . '</td>';
        $body .= '      <td style="padding: 10px;">' . $user['expired_at'] . '</td>';
        $body .= '  </tr>';
    }

    $body .= '</table>';

    wp_mail($to, $subj, $body, $headers);
}
