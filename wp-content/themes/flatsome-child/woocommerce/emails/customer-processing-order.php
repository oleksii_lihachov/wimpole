<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<div style='display:flex'><img style="margin: 0 auto" src="<?php echo 'https://wimpolepharmacy.co.uk/wp-content/uploads/2021/06/Asset-3wimpole_logo.png'; ?>" /></div>

<?php /* translators: %s: Customer first name */ ?>
<p><?php printf( esc_html__( 'Welcome %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
<p><?php printf( esc_html__( 'We have received your order and we will proceed with it soon.', 'woocommerce' ) ); ?></p>
<p><?php printf( esc_html__( 'Thanks for shopping with us at Wimpole Pharmacy! Be sure to visit again soon.', 'woocommerce' ) ); ?></p>
<p><?php printf( esc_html__( 'See you soon.', 'woocommerce' ) ); ?></p>
<p><?php printf( esc_html__( 'Your friends at Wimpole Pharmacy', 'woocommerce' ) ); ?></p>
