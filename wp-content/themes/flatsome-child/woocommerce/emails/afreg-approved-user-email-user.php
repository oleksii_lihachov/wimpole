<?php
/**
 * New User Registration notification to user
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/afreg-approved-user-email-user.php.
 *
 */

defined( 'ABSPATH' ) || exit;

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<div style='display:flex; margin-bottom: 10px;'><img style="margin: 0 auto" src="<?php echo 'https://wimpolepharmacy.co.uk/wp-content/uploads/2021/06/Asset-3wimpole_logo.png'; ?>" /></div>

<p>
	<?php
		
	   $content = wpautop( wptexturize( $email_content ) );

		echo wp_kses_post( $content );
	?>
</p>
<?php

/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
