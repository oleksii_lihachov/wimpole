<?php

/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if (!defined('ABSPATH')) {
    exit;
}

$wrapper_classes = array();
$row_classes     = array();
$main_classes    = array();
$sidebar_classes = array();

$layout = get_theme_mod('checkout_layout');

if (!$layout) {
    $sidebar_classes[] = 'has-border';
}

if ($layout == 'simple') {
    $sidebar_classes[] = 'is-well';
}

$wrapper_classes = implode(' ', $wrapper_classes);
$row_classes     = implode(' ', $row_classes);
$main_classes    = implode(' ', $main_classes);
$sidebar_classes = implode(' ', $sidebar_classes);

do_action('woocommerce_before_checkout_form', $checkout);

// If checkout registration is disabled and not logged in, the user cannot checkout.
if (!$checkout->is_registration_enabled() && $checkout->is_registration_required() && !is_user_logged_in()) {
    echo esc_html(apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce')));
    return;
}

// Social login.
if (flatsome_option('facebook_login_checkout') && get_option('woocommerce_enable_myaccount_registration') == 'yes' && !is_user_logged_in()) {
    wc_get_template('checkout/social-login.php');
}
?>

<form name="checkout" method="post" class="checkout woocommerce-checkout <?php echo esc_attr($wrapper_classes); ?>" action="<?php echo esc_url(wc_get_checkout_url()); ?>" enctype="multipart/form-data">
    <div class="row pt-0 <?php echo esc_attr($row_classes); ?>">
        <div class="<?php if (is_page('Checkout')) { ?>large-7 <?php } else if (is_page('patient-questionnaire')) { ?> large-12 <?php } ?> col <?php echo esc_attr($main_classes); ?>">

            <?php if ($checkout->get_checkout_fields()) : ?>
                <?php do_action('woocommerce_checkout_before_customer_details'); ?>
                <div id="customer_details">
                    <div class="clear">
                        <?php do_action('woocommerce_checkout_billing'); ?>
                    </div>
                    <div class="clear">
                        <?php do_action('woocommerce_checkout_shipping'); ?>
                    </div>
                </div>
                <?php do_action('woocommerce_checkout_after_customer_details'); ?>
            <?php endif; ?>

        </div>
        <?php if (is_page('Checkout')) { ?>
            <div class="large-5 col">
                <?php flatsome_sticky_column_open('checkout_sticky_sidebar'); ?>
                <div class="col-inner">
                    <div class="checkout-sidebar sm-touch-scroll">
                        <?php do_action('woocommerce_checkout_before_order_review_heading'); ?>
                        <h3 id="order_review_heading"><?php esc_html_e('Your order', 'woocommerce'); ?></h3>
                        <?php do_action('woocommerce_checkout_before_order_review'); ?>
                        <div id="order_review" class="woocommerce-checkout-review-order">
                            <?php do_action('woocommerce_checkout_order_review'); ?>
                        </div>
                        <?php do_action('woocommerce_checkout_after_order_review'); ?>
                    </div>
                </div>
                <?php flatsome_sticky_column_close('checkout_sticky_sidebar'); ?>
            </div>
        <?php } else if (is_page('patient-questionnaire')) {
            $url = strpos($_SERVER['REQUEST_URI'], 'staging') !== false ? '/staging/?page_id=57' : '/checkout';
        ?>
            <div class="large-6 col">
                <a href="<?php echo $url; ?>" class="button checkout wc-forward" id="save-customer-form"><?php echo esc_html__('Complete Your Order', 'woocommerce'); ?></a>
            </div>
        <?php } ?>
    </div>
</form>

<?php do_action('woocommerce_after_checkout_form', $checkout); ?>
<?php //echo apply_filters( 'woocommerce_order_button_html', '<button type="submit" class="button alt secondary dark" name="woocommerce_checkout_place_order" id="place_order" value="' . esc_attr( $order_button_text ) . '" data-value="' . esc_attr( $order_button_text ) . '">' . esc_html( $order_button_text ) . '</button>' ); // @codingStandardsIgnoreLine
?>

<script>
    jQuery(document).ready(function($) {
        $('#save-customer-form').on('click', function(ev) {
            ev.preventDefault();
            var data = $('form[name="checkout"]').serializeArray();

            $.ajax({
                method: "GET",
                url: window.wc_add_to_cart_params.ajax_url,
                data: {
                    action: 'save_customer_checkout_form',
                    form_data: data,
                    current_user: '<?php echo get_current_user_id(); ?>',
                },
                success: function(res) {
                    $('.woocommerce-error.message-wrapper').html('');
                    $('.woocommerce-error.message-wrapper').hide();

                    if (res.success) {
                        var url = window.location.href.includes('staging') ? '/staging/?page_id=57' : '/checkout';
                        window.location.href = url;
                    } else {
                        var errorsHtml = '';
                        for (var i = 0; i < res.data.length; i++) {
                            errorsHtml += '<li>';
                            errorsHtml += '<div class="message-container container alert-color medium-text-center">';
                            errorsHtml += res.data[i];
                            errorsHtml += '</div>';
                            errorsHtml += '</li>';
                        }
                        $('.woocommerce-error.message-wrapper').html(errorsHtml);
                        $('.woocommerce-error.message-wrapper').show();
                        $('.woocommerce-error.message-wrapper').get(0).scrollIntoView({
                            block: "center",
                            behavior: "smooth"
                        });
                    }
                },
                error: function(res) {
                    console.log(res);
                }
            });
        });
    });
</script>


<?php
