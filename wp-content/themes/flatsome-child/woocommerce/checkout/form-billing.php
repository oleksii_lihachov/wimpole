<?php

/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined('ABSPATH') || exit;
?>
<div class="woocommerce-billing-fields">
    <?php if (is_page('patient-questionnaire')) : ?>
        <ul class="woocommerce-error message-wrapper" role="alert" style="display: none;"></ul>
        <p>The information gathered here is an industry requirement for all orders of prescribed products. It is a simple patient assessment questionnaire to assist our prescribing team to ensure your patients welfare. The questionnaire should take no more than 1 minute and if you need help please use our chat support.</p>
        <p>The information is not shared with any other companies nor is it used for marketing purposes.</p>
    <?php endif; ?>
    <?php if (wc_ship_to_billing_address_only() && WC()->cart->needs_shipping()) : ?>

        <h5><?php esc_html_e('Billing &amp; Shipping', 'woocommerce'); ?></h5>

    <?php else : ?>

        <?php if (is_page('Checkout')) { ?>
            <h5><?php esc_html_e('Customer Information', 'woocommerce'); ?></h5>
        <?php } ?>

    <?php endif; ?>

    <!-- Show additional field just for patient page -->
    <?php if (is_page('patient-questionnaire')) {
        do_action('woocommerce_before_checkout_billing_form', $checkout);
    } else {
    ?>
        <div style="display: none;">
            <?php do_action('woocommerce_before_checkout_billing_form', $checkout); ?>
        </div>
    <?php
    }
    // end: Show additional field just for patient page -->

    ?>

    <div class="woocommerce-billing-fields__field-wrapper <?php if (is_page('Checkout')) { ?> woocommerce-checkout-page<?php } ?>">
        <?php
        $fields = $checkout->get_checkout_fields('billing');

        foreach ($fields as $key => $field) {
            woocommerce_form_field($key, $field, $checkout->get_value($key));
        }
        ?>
    </div>


    <?php if (!is_page('patient-questionnaire')) {
        do_action('woocommerce_after_checkout_billing_form', $checkout);
    }?>
</div>

<?php if (!is_user_logged_in() && $checkout->is_registration_enabled()) : ?>
    <div class="woocommerce-account-fields">
        <?php if (!$checkout->is_registration_required()) : ?>

            <p class="form-row form-row-wide create-account">
                <label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
                    <input class="woocommerce-form__input woocommerce-form__input-checkbox input-checkbox" id="createaccount" <?php checked((true === $checkout->get_value('createaccount') || (true === apply_filters('woocommerce_create_account_default_checked', false))), true); ?> type="checkbox" name="createaccount" value="1" /> <span><?php esc_html_e('Create an account?', 'woocommerce'); ?></span>
                </label>
            </p>

        <?php endif; ?>

        <?php do_action('woocommerce_before_checkout_registration_form', $checkout); ?>

        <?php if ($checkout->get_checkout_fields('account')) : ?>

            <div class="create-account">
                <?php foreach ($checkout->get_checkout_fields('account') as $key => $field) : ?>
                    <?php woocommerce_form_field($key, $field, $checkout->get_value($key)); ?>
                <?php endforeach; ?>
                <div class="clear"></div>
            </div>

        <?php endif; ?>

        <?php do_action('woocommerce_after_checkout_registration_form', $checkout); ?>
    </div>
<?php endif; ?>