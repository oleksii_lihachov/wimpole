<?php

/**
 * Add custom Theme Functions here.
 *
 * @package null
 */

/**
 * Enqueue scripts and styles.
 */
function flatsome_child_scripts()
{
    // @codingStandardsIgnoreLine
    wp_enqueue_style(
        'flatsome_child_style',
        get_stylesheet_directory_uri() . '/dist/style.css',
        null,
        '1.0'
    );

    // @codingStandardsIgnoreLine
    wp_enqueue_script(
        'flatsome_child_script',
        get_stylesheet_directory_uri() . '/dist/bundle.js',
        ['jquery'],
        '1.0',
        true
    );
}

add_action('wp_enqueue_scripts', 'flatsome_child_scripts');

add_action('wp_enqueue_scripts', function () {
    $uri = get_template_directory_uri();
    $theme = wp_get_theme(get_template());
    $version = $theme->get('Version');

    wp_dequeue_script('flatsome-js');

    wp_enqueue_script('flatsome-js', get_stylesheet_directory_uri() . '/assets/js/flatsome.js', [
        'jquery',
        'hoverIntent',
    ], $version, true);
}, 100);

/**
 * Customer Reviews shortcode
 *
 * @param mixed $atts attributes.
 * @return string
 */
function customer_reviews_func($atts)
{
    if (!is_array($atts)) {
        $atts = [];
    }

    // Repeater styles.
    $repater['id'] = 'customer-reviews-' . rand();
    $repater['title'] = '';
    $repater['tag'] = 'customer_reviews';
    $repater['class'] = 'c-customer-reviews';
    $repater['visibility'] = '';
    $repater['type'] = 'slider';
    $repater['style'] = 'default';
    $repater['slider_style'] = 'reveal';
    $repater['slider_nav_color'] = '';
    $repater['slider_nav_position'] = '';
    $repater['slider_bullets'] = true;
    $repater['auto_slide'] = '';
    $repater['row_spacing'] = 'small';
    $repater['row_width'] = 'full-width';
    $repater['columns'] = '5';
    $repater['columns__md'] = '';
    $repater['columns__sm'] = '';
    $repater['filter'] = '';
    $repater['depth'] = false;
    $repater['depth_hover'] = false;
    $args = ['post_type' => 'product'];
    $comments = get_comments($args);

    if (empty($comments)) {
        return '';
    }

    ob_start();

    get_flatsome_repeater_start($repater);
    foreach ($comments as $comment) {
        $stars = get_comment_meta($comment->comment_ID, 'rating', true);
        $star_row = '';

        $content = $comment->comment_content;
        $excerpt = $content;

        if (strlen($content) > 100) {
            $excerpt = strip_shortcodes($content);
            // @codingStandardsIgnoreLine
            // @codingStandardsIgnoreLine
            // @codingStandardsIgnoreLine
            $excerpt = strip_tags($excerpt);
            $excerpt = substr($excerpt, 0, 100) . '...';
        }

        if ('1' === $stars) {
            $star_row = '<div class="star-rating"><span style="width:25%"><strong class="rating"></strong></span></div>';
        } elseif ('2' === $stars) {
            $star_row = '<div class="star-rating"><span style="width:35%"><strong class="rating"></strong></span></div>';
        } elseif ('3' === $stars) {
            $star_row = '<div class="star-rating"><span style="width:55%"><strong class="rating"></strong></span></div>';
        } elseif ('4' === $stars) {
            $star_row = '<div class="star-rating"><span style="width:75%"><strong class="rating"></strong></span></div>';
        } elseif ('5' === $stars) {
            $star_row = '<div class="star-rating"><span style="width:100%"><strong class="rating"></strong></span></div>';
        }

        ?>
		<div class="col c-customer-reviews">
			<div class="col-inner">
				<div class="box box-text-bottom has-hover has-shadow">
					<div class="box-text">
						<div class="box-text-inner">
							<h5><?php echo esc_html($comment->comment_author); ?></h5>
							<?php
                                    // @codingStandardsIgnoreLine
                                    // @codingStandardsIgnoreLine
                                    // @codingStandardsIgnoreLine
                                    echo $star_row;
        ?>
							<div class="is-divider"></div>
							<div class="clearfix">
								<div class="c-customer-reviews__content">
									<p><?php echo esc_html($excerpt); ?></p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
    }

    get_flatsome_repeater_end($repater);

    $content = ob_get_contents();
    ob_end_clean();

    return $content;
}

add_shortcode('customer_reviews', 'customer_reviews_func');

if (!function_exists('flatsome_woocommerce_shop_loop_category')) {
    /**
     * Add and/or Remove Categories
     */
    function flatsome_woocommerce_shop_loop_category()
    {
        if (!flatsome_option('product_box_category')) {
            return;
        }
        ?>
		<p class="category is-smaller no-text-overflow product-cat op-7">
			<?php
                global $product;
        $product_cats = function_exists('wc_get_product_category_list') ? wc_get_product_category_list(get_the_ID(), '\n', '', '') : $product->get_categories('\n', '', '');
        $product_cats = strip_tags($product_cats);

        if ($product_cats) {
            list($first_part) = explode('\n', $product_cats);
            echo esc_html(apply_filters('flatsome_woocommerce_shop_loop_category', $first_part, $product));
        }
        ?>
		</p>
	<?php
    }
}
add_action('woocommerce_shop_loop_item_title', 'flatsome_woocommerce_shop_loop_category', 0);

// Allow SVG
add_filter(
    'wp_check_filetype_and_ext',
    function ($data, $file, $filename, $mimes) {
        global $wp_version;
        if ($wp_version !== '4.7.1') {
            return $data;
        }

        $filetype = wp_check_filetype($filename, $mimes);

        return [
            'ext' => $filetype['ext'],
            'type' => $filetype['type'],
            'proper_filename' => $data['proper_filename'],
        ];
    },
    10,
    4
);

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}

add_filter('upload_mimes', 'cc_mime_types');

function fix_svg()
{
    echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
             width: 100% !important;
             height: auto !important;
        }
        </style>';
}

add_action('admin_head', 'fix_svg');

add_filter(
    'woocommerce_registration_error_email_exists',
    function ($text, $email) {
        return __('An account is already registered with your email address. <a href="#" data-open="#login-form-popup" class="showlogin">Please log in.</a>', 'flatsome-child');
    },
    10,
    2
);

/**
 * @snippet       Deny Login Upon Registration @ WooCommerce My Account
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 3.7
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */

add_filter('woocommerce_registration_auth_new_customer', '__return_false');

//function filter_woocommerce_email_headers($header, $email_id, $order)
//{
//    // Compare
//    if ($email_id == 'customer_on_hold_order') {
//        // Prepare the the data
//        $formatted_email = '<' . get_post_meta($order->get_id(), '_billing_email2', true) . '>';
//        // Add Bcc to headers
//        $header .= 'Bcc: ' . $formatted_email . '\r\n';
//    }
//
//    return $header;
//}

//add_filter('woocommerce_email_headers', 'filter_woocommerce_email_headers', 10, 3);

function skynix_send_order_data_to_customer($recipient, $order)
{
    if ($order) {
        $email = get_post_meta($order->get_id(), '_billing_email2', true);
        return $recipient . ', ' . $email;
    }

    return $recipient;
}

//add_filter('woocommerce_email_recipient_new_order', 'skynix_send_order_data_to_customer', 10, 2);

// Add email meta box to order admin.
add_action('add_meta_boxes', function ($order) {
    add_meta_box(
        'patient_details_email_fields',
        __('Send Patient Details', 'woocommerce'),
        'patient_details_email_fields',
        'shop_order',
        'side',
        'core'
    );
});

function patient_details_email_fields()
{
    ?>
	<p>
		<input type="email" placeholder="Patient Email" name="patient_details_email_sender" />
		<button type="button" onclick="document.post.submit();" class="button button-primary" style="float: right;">Send</button>
	</p>

	<?php
}

add_action('save_post', function ($post_id) {
    if (isset($_POST['patient_details_email_sender'])) {
        $email = $_POST['patient_details_email_sender'];

        $mailer = WC()->mailer();
        $mails = $mailer->get_emails();
        if (!empty($mails)) {
            foreach ($mails as $mail) {
                echo $mail->id;
                echo '<br>';
                if ($mail->id == 'wc_patient_details') {
                    $mail->trigger($post_id, $email);
                }
            }
        }
    }
}, 10, 1);

// Validate Custom fields "Custom User Registration Fields for WooCommerce" plugin in YITH checkout login form
add_action('yith_welrp_before_register_action', function ($data) {
    $afreg_args = [
        'posts_per_page' => -1,
        'post_type' => 'afreg_fields',
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'suppress_filters' => false,
        'order' => 'ASC'
    ];

    $afreg_extra_fields = get_posts($afreg_args);

    if (empty($afreg_extra_fields)) {
        return '';
    }

    foreach ($afreg_extra_fields as $afreg_field) {
        $afreg_field_show_in_registration_form = get_post_meta($afreg_field->ID, 'afreg_field_show_in_registration_form', true);
        $afreg_field_required = get_post_meta(intval($afreg_field->ID), 'afreg_field_required', true);
        $afreg_field_type = get_post_meta(intval($afreg_field->ID), 'afreg_field_type', true);

        if ($afreg_field_required === 'off') {
            continue;
        }
        if ($afreg_field_show_in_registration_form === 'off') {
            continue;
        }

        if (!isset($_POST['afreg_additional_' . $afreg_field->ID])) {
            throw new Exception(__('Please fill required fields', 'yith-easy-login-register-popup-for-woocommerce'));
        }

        if ('fileupload' === $afreg_field_type && empty($_POST['afreg_additional_' . $afreg_field->ID])) {
            throw new Exception(__('Please fill required fields', 'yith-easy-login-register-popup-for-woocommerce'));
        }

        if ('datepicker' === $afreg_field_type && empty($_POST['afreg_additional_' . $afreg_field->ID])) {
            throw new Exception(__('Please fill required fields', 'yith-easy-login-register-popup-for-woocommerce'));
        }
    }
});

// When user register from car page popup (YITH login plugin)
// Need manually save uploaded images
// Process this after customer created and before email to admin sends
// in simple registration file append to $_FILES
// in our case it's hidden field with base64 encoded image
add_action('afreg_new_user_email_notification_admin', function ($customer_id, $def_fiels_email_fields) {
    $afreg_args = [
        'posts_per_page' => -1,
        'post_type' => 'afreg_fields',
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'suppress_filters' => false,
        'order' => 'ASC'
    ];

    $afreg_extra_fields = get_posts($afreg_args);

    if (empty($afreg_extra_fields)) {
        return;
    }

    foreach ($afreg_extra_fields as $afreg_field) {
        $afreg_field_type = get_post_meta(intval($afreg_field->ID), 'afreg_field_type', true);

        if ('fileupload' === $afreg_field_type) {
            // Here is base64 from YITH Login
            // Manually upload file and write meta field
            if (!empty($_POST['afreg_additional_' . intval($afreg_field->ID)])) {
                $upload_dir = wp_upload_dir();
                $upload_path = $upload_dir['basedir'] . '/addify_registration_uploads/';
                $image_parts = explode(';base64,', $_POST['afreg_additional_' . $afreg_field->ID]);
                $decoded = base64_decode($image_parts[1]);
                $filename = uniqid() . '.png';

                file_put_contents($upload_path . $filename, $decoded);
                update_user_meta($customer_id, 'afreg_additional_' . intval($afreg_field->ID), $filename);
            }
        }
    }
}, 1, 2);

add_action('post_edit_form_tag', 'post_edit_form_tag');

function post_edit_form_tag()
{
    echo ' enctype="multipart/form-data"';
}

// Add Custom fields "Custom User Registration Fields for WooCommerce" plugin to YITH checkout login form
function show_afred_fields()
{
    $afreg_args = [
        'posts_per_page' => -1,
        'post_type' => 'afreg_fields',
        'post_status' => 'publish',
        'orderby' => 'menu_order',
        'suppress_filters' => false,
        'order' => 'ASC'
    ];

    $afreg_extra_fields = get_posts($afreg_args);

    if (empty($afreg_extra_fields)) {
        return '';
    }

    foreach ($afreg_extra_fields as $afreg_field) {
        $afreg_field_type = get_post_meta(intval($afreg_field->ID), 'afreg_field_type', true);
        $afreg_field_options = unserialize(get_post_meta(intval($afreg_field->ID), 'afreg_field_option', true));
        $afreg_field_required = get_post_meta(intval($afreg_field->ID), 'afreg_field_required', true);
        $afreg_field_width = get_post_meta(intval($afreg_field->ID), 'afreg_field_width', true);
        $afreg_field_show_in_registration_form = get_post_meta($afreg_field->ID, 'afreg_field_show_in_registration_form', true);

        if (!empty(get_post_meta(intval($afreg_field->ID), 'afreg_field_placeholder', true))) {
            $afreg_field_placeholder = get_post_meta(intval($afreg_field->ID), 'afreg_field_placeholder', true);
        } else {
            $afreg_field_placeholder = '';
        }

        $afreg_field_description = get_post_meta(intval($afreg_field->ID), 'afreg_field_description', true);
        $afreg_field_css = get_post_meta(intval($afreg_field->ID), 'afreg_field_css', true);
        $afreg_field_heading_type = get_post_meta(intval($afreg_field->ID), 'afreg_field_heading_type', true);
        $afreg_field_description_field = get_post_meta(intval($afreg_field->ID), 'afreg_field_description_field', true);

        if (!empty($afreg_field_width) && 'full' == $afreg_field_width) {
            $afreg_main_class = 'form-row-wide newr';
        } elseif (!empty($afreg_field_width) && 'half' == $afreg_field_width) {
            $afreg_main_class = 'half_width newr';
        }

        if (!empty(get_post_meta(intval($afreg_field->ID), 'afreg_is_dependable', true))) {
            $afreg_is_dependable = get_post_meta(intval($afreg_field->ID), 'afreg_is_dependable', true);
        } else {
            $afreg_is_dependable = 'off';
        }

        $afreg_field_user_roles = get_post_meta($afreg_field->ID, 'afreg_field_user_roles', true);
        $field_roles = unserialize($afreg_field_user_roles);

        if ('text' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                            if (!empty($afreg_field->post_title)) {
                                echo esc_html__($afreg_field->post_title, 'addify_reg');
                            }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>

					</span></label>
				<input type="text" class="input-text 
										<?php
                            if (!empty($afreg_field_css)) {
                                echo esc_attr($afreg_field_css);
                            }
            ?>
										" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>" value="<?php echo esc_attr($vall); ?>" placeholder="<?php echo esc_html__($afreg_field_placeholder, 'addify_reg'); ?>" />
				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p>
		<?php
        } elseif ('textarea' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                        if (!empty($afreg_field->post_title)) {
                            echo esc_html__($afreg_field->post_title, 'addify_reg');
                        }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>
				<textarea class="input-text 
										<?php
                            if (!empty($afreg_field_css)) {
                                echo esc_attr($afreg_field_css);
                            }
            ?>
										" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>"><?php echo esc_attr($vall); ?></textarea>
				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p>
		<?php
        } elseif ('select' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                        if (!empty($afreg_field->post_title)) {
                            echo esc_html__($afreg_field->post_title, 'addify_reg');
                        }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>
				<select class="input-select 
										<?php
                            if (!empty($afreg_field_css)) {
                                echo esc_attr($afreg_field_css);
                            }
            ?>
										" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php foreach ($afreg_field_options as $afreg_field_option) { ?>
						<option value="<?php echo esc_attr($afreg_field_option['field_value']); ?>" <?php echo selected($afreg_field_option['field_value'], $vall); ?>>
							<?php
                            if (!empty($afreg_field_option['field_text'])) {
                                echo esc_html__(esc_attr($afreg_field_option['field_text']), 'addify_reg');
                            }
					    ?>
						</option>
					<?php } ?>
				</select>
				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p>
		<?php
        } elseif ('multiselect' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                        if (!empty($afreg_field->post_title)) {
                            echo esc_html__($afreg_field->post_title, 'addify_reg');
                        }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>
				<select class="input-select <?php echo esc_attr($afreg_field_css); ?>" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>[]" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>" multiple>
					<?php
                    foreach ($afreg_field_options as $afreg_field_option) {
                        //For Multiselect
                        if (is_array($afreg_field_option['field_value']) && in_array(esc_attr($afreg_field_option['field_value']), $_POST['afreg_additional_' . intval($afreg_field->ID)])) {
                            $vall_se = 'selected';
                        } else {
                            $vall_se = '';
                        }

                        ?>


						<option value="<?php echo esc_attr($afreg_field_option['field_value']); ?>" <?php echo esc_attr($vall_se); ?>>
							<?php echo esc_html__(esc_attr($afreg_field_option['field_text']), 'addify_reg'); ?>
						</option>
					<?php } ?>
				</select>
				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p><?php
        } elseif ('multi_checkbox' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                if (!empty($afreg_field->post_title)) {
                    echo esc_html__($afreg_field->post_title, 'addify_reg');
                }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>



				<?php foreach ($afreg_field_options as $afreg_field_option) { ?>
					<input type="checkbox" class="input-checkbox 
											<?php
                                if (!empty($afreg_field_css)) {
                                    echo esc_attr($afreg_field_css);
                                }
				    ?>
											" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>[]" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>" value="<?php echo esc_attr($afreg_field_option['field_value']); ?>" <?php
				                                                                                                                                                                                                            if (in_array($afreg_field_option['field_value'], $vall_checkbox)) {
				                                                                                                                                                                                                                echo 'checked';
				                                                                                                                                                                                                            }
				    ?> />
					<span class="afreg_radios">
						<?php
                        if (!empty($afreg_field_option['field_text'])) {
                            echo esc_html__(esc_attr($afreg_field_option['field_text']), 'addify_reg');
                        }
				    ?>
					</span>
				<?php } ?>


				<?php if (!empty($afreg_field_description)) { ?>
					<br>
					<span class="afreg_field_message"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p>
		<?php
        } elseif ('checkbox' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                        if (!empty($afreg_field->post_title)) {
                            echo esc_html__($afreg_field->post_title, 'addify_reg');
                        }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>




				<input <?php echo checked('yes', esc_attr($vall)); ?> type="checkbox" class="input-checkbox 
									<?php
                        if (!empty($afreg_field_css)) {
                            echo esc_attr($afreg_field_css);
                        }
            ?>
									" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>" value="yes" />

				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p>
		<?php
        } elseif ('radio' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                        if (!empty($afreg_field->post_title)) {
                            echo esc_html__($afreg_field->post_title, 'addify_reg');
                        }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>

				<?php foreach ($afreg_field_options as $afreg_field_option) { ?>
					<input type="radio" class="input-radio 
											<?php
                                if (!empty($afreg_field_css)) {
                                    echo esc_attr($afreg_field_css);
                                }
				    ?>
											" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>" value="<?php echo esc_attr($afreg_field_option['field_value']); ?>" <?php echo checked($afreg_field_option['field_value'], $vall); ?> />
					<span class="afreg_radio">
						<?php
                        if (!empty($afreg_field_option['field_text'])) {
                            echo esc_html__(esc_attr($afreg_field_option['field_text']), 'addify_reg');
                        }
				    ?>
					</span>
				<?php } ?>

				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message_radio"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p><?php
        } elseif ('number' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                if (!empty($afreg_field->post_title)) {
                    echo esc_html__($afreg_field->post_title, 'addify_reg');
                }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>
				<input type="number" class="input-text 
										<?php
                            if (!empty($afreg_field_css)) {
                                echo esc_attr($afreg_field_css);
                            }
            ?>
										" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>" value="<?php echo esc_attr($vall); ?>" placeholder="<?php echo esc_html__($afreg_field_placeholder, 'addify_reg'); ?>" />
				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p>
		<?php
        } elseif ('password' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                        if (!empty($afreg_field->post_title)) {
                            echo esc_html__($afreg_field->post_title, 'addify_reg');
                        }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>
				<input type="password" class="input-text 
										<?php
                            if (!empty($afreg_field_css)) {
                                echo esc_attr($afreg_field_css);
                            }
            ?>
										" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>" value="<?php echo esc_attr($vall); ?>" placeholder="<?php echo esc_html__($afreg_field_placeholder, 'addify_reg'); ?>" />
				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p>
		<?php
        } elseif ('fileupload' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                        if (!empty($afreg_field->post_title)) {
                            echo esc_html__($afreg_field->post_title, 'addify_reg');
                        }
            ?>
					<input type="hidden" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>" />
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>
				<input type="file" accept="image/png, image/gif, image/jpeg" class="input-text 
										<?php
                            if (!empty($afreg_field_css)) {
                                echo esc_attr($afreg_field_css);
                            }
            ?>
										" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>" value="" placeholder="<?php echo esc_html__($afreg_field_placeholder, 'addify_reg'); ?>" />
				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p><?php
        } elseif ('datepicker' == $afreg_field_type && 'off' != $afreg_field_show_in_registration_form) {
            ?>
			<p class="form-row <?php echo esc_attr($afreg_main_class); ?>" id="afreg_additionalshowhide_<?php echo intval($afreg_field->ID); ?>">
				<label for="afreg_additional_<?php echo intval($afreg_field->ID); ?>">
					<?php
                if (!empty($afreg_field->post_title)) {
                    echo esc_html__($afreg_field->post_title, 'addify_reg');
                }
            ?>
					<span class="required">
						<?php
                if (!empty($afreg_field_required) && 'on' == $afreg_field_required) {
                    ?>
							*
						<?php
                }
            ?>
					</span></label>
				<input type="date" class="input-text  
										<?php
                            if (!empty($afreg_field_css)) {
                                echo esc_attr($afreg_field_css);
                            }
            ?>
										" name="afreg_additional_<?php echo intval($afreg_field->ID); ?>" id="afreg_additional_<?php echo intval($afreg_field->ID); ?>" value="<?php echo esc_attr($vall); ?>" placeholder="<?php echo esc_html__($afreg_field_placeholder, 'addify_reg'); ?>" />
				<?php if (!empty($afreg_field_description)) { ?>
					<span class="afreg_field_message_radio"><?php echo wp_kses_post($afreg_field_description, 'addify_reg'); ?></span>
				<?php } ?>
			</p>
<?php
        }
    }
}

require_once 'inc/multi-checkout.php';
new MultiCheckout();

//Disable XML-RPC
// https://www.sonarsource.com/blog/wordpress-core-unauthenticated-blind-ssrf/

add_filter('xmlrpc_methods', function ($methods) {
    if (isset($methods['pingback.ping'])) {
        unset($methods['pingback.ping']);
    }

    if (isset($methods['pingback.extensions.getPingbacks'])) {
        unset($methods['pingback.extensions.getPingbacks']);
    }

    return $methods;
});

// add_role(
//     'customer_connect', //  System name of the role.
//     __( 'Customer Connect'  ), // Display name of the role.
//     array(
//         'read'  => true,
//     )
// );

// add_filter( 'redirect_canonical', 'disable_redirect_canonical_for_terms', 2, 1 );
// function disable_redirect_canonical_for_terms( $redirect_url, $requested_url ) {
// 	if (current_user_can('administrator')) {
// 		var_dump($requested_url);
// 	}
//     // if( is_account_page() ){
//     //     return false;
//     // }
//     // return $redirect_url;
// 	return false;
// }

add_action('template_redirect', function () {
    global $wp;
    global $wp_query;

    if ($wp_query->query_vars['lost-password'] && !empty($wp_query->query_vars['lost-password'])) {
        wp_redirect(home_url());
        exit;
    }

    // if ($wp_query->query_vars['order-received'] && !empty($wp_query->query_vars['order-received'])) {
    // 	wp_redirect( home_url() );
    // 	exit;
    // }

    // if (current_user_can('administrator')) {
    // 	var_dump($wp_query->query_vars);
    // 	// var_dump($wp_query->query_vars['lost-password']);
    // }
});




require_once 'inc/users-export.php';
new UsersExport();
