<?php

class MultiCheckout
{
    public function __construct()
    {
        // Custom Checkout button
        add_action('woocommerce_widget_shopping_cart_buttons', [$this, 'changeCheckoutUrls'], 10);
        // Remove unnessesary checkout fields from patient page
        add_action('template_redirect', [$this, 'changeQuestionnaire']);
        // Hide coupon form on patient page (checkout)
        add_filter('woocommerce_coupons_enabled', [$this, 'hideCoupon'], 10, 1);

        add_action('wp_ajax_save_customer_checkout_form', [$this, 'save_customer_checkout_form_callback']);
        add_action('wp_ajax_nopriv_save_customer_checkout_form', [$this, 'save_customer_checkout_form_callback']);

        // Make Postcode field optional
        // add_filter('woocommerce_default_address_fields', 'QuadLayers_optional_postcode_checkout');
        // function QuadLayers_optional_postcode_checkout($p_fields)
        // {
        //     $p_fields['postcode']['required'] = false;
        //     return $p_fields;
        // }
    }

    public function changeCheckoutUrls()
    {
        // Removing Buttons
        remove_action('woocommerce_widget_shopping_cart_buttons', 'woocommerce_widget_shopping_cart_proceed_to_checkout', 20);
        // Adding customized Buttons
        add_action('woocommerce_widget_shopping_cart_buttons', [$this, 'custom_widget_shopping_cart_proceed_to_checkout'], 20);
    }

    public function custom_widget_shopping_cart_proceed_to_checkout()
    {
        if (current_user_can('administrator') || current_user_can('customer_clone') || current_user_can('prescriber') || current_user_can('customer_connect') || current_user_can('hair_plus')) {
            $url = strpos($_SERVER['REQUEST_URI'], 'staging') !== false ? '/staging/?page_id=6952' : '/patient-questionnaire';
            echo '<a href="' . $url . '" class="button checkout wc-forward">' . esc_html__('Checkout', 'woocommerce') . '</a>';
        } else {
            $url = strpos($_SERVER['REQUEST_URI'], 'staging') !== false ? '/staging/?page_id=57' : '/checkout';
            echo '<a href="' . $url . '" class="button checkout wc-forward">' . esc_html__('Checkout', 'woocommerce') . '</a>';
        }
    }

    public function changeQuestionnaire()
    {
        if (is_page('patient-questionnaire')) {
            add_filter('woocommerce_checkout_fields', '__return_false');
            add_filter('woocommerce_cart_needs_shipping_address', '__return_false');
            add_filter('woocommerce_enable_order_notes_field', '__return_false');
        }
    }

    public function hideCoupon($value)
    {
        if (is_admin()) {
            return true;
        }
        global $post;
		
        if (!$post) {
            return $value;
        }

        $post_slug = $post->post_name;

        if ('patient-questionnaire' === $post_slug) {
            $enabled = false;
        } else {
            $enabled = true;
        }
        return $enabled;
    }

    public function get_checkout_fields()
    {
        $user_role = is_user_logged_in() ? current(wp_get_current_user()->roles) : 'guest';

        $af_cf_args = [
            'numberposts' => -1,
            'post_type' => 'af_ch_fields',
            'post_status' => 'publish',
            'orderby' => 'menu_order',
            'suppress_filters' => false,
            'order' => 'ASC',
            'fields' => 'ids',
        ];

        $af_cf_extra_fields = get_posts($af_cf_args);

        $checkout_fields = [];

        foreach ($af_cf_extra_fields as $field_id) {
            $field_roles = get_post_meta($field_id, 'af_ch_f_field_user_roles', true);

            $is_dependent = get_post_meta($field_id, 'af_ch_f_dependable', true);

            if ('yes' == $is_dependent) {
                continue;
            }

            if (!empty($field_roles) && !in_array($user_role, $field_roles)) {
                continue;
            }

            $checkout_fields[] = $field_id;

            $dependent_fields = $this->get_dependent_field($field_id);

            if (!empty($dependent_fields)) {
                foreach ($dependent_fields as $dep_field) {
                    $checkout_fields[] = $dep_field;
                }
            }
        }

        return $checkout_fields;
    }

    public function get_all_dependent_fields()
    {
        $args = [
            'posts_per_page' => -1,
            'post_type' => 'af_ch_fields',
            'post_status' => 'publish',
            'orderby' => 'menu_order',
            'suppress_filters' => false,
            'order' => 'ASC',
            'fields' => 'ids',
            'meta_key' => 'af_ch_f_dependable',
            'meta_value' => 'yes',
        ];

        $fields = get_posts($args);

        $user_role = is_user_logged_in() ? current(wp_get_current_user()->roles) : 'guest';

        if (!empty($fields)) {
            foreach ($fields as $key => $field_id) {
                $field_roles = get_post_meta($field_id, 'af_ch_f_field_user_roles', true);

                if (!empty($field_roles)) {
                    if (!in_array($user_role, $field_roles)) {
                        unset($fields[$key]);
                    }
                }
            }
        }

        return $fields;
    }

    public function get_dependent_field($field_id)
    {
        if (empty($this->dependent_fields)) {
            $this->dependent_fields = $this->get_all_dependent_fields();
        }

        if (empty($this->dependent_fields)) {
            return;
        }

        $dependent_fields = [];

        foreach ($this->dependent_fields as $field) {
            if (get_post_meta($field, 'af_ch_f_dep_fields', true) == $field_id) {
                $dependent_fields[] = $field;
            }
        }

        return $dependent_fields;
    }

    public function save_customer_checkout_form_callback()
    {
        $user_id = $_GET['current_user'];
        $form_data = $_GET['form_data'];

        $checkout_fields = $this->get_checkout_fields();
        $errors = [];

        foreach ($checkout_fields as $field_id) {
            $_required = get_post_meta($field_id, 'af_ch_f_field_required', true);
            $field_name = 'af_ch_f_' . $field_id;
            $_type = get_post_meta($field_id, 'af_ch_f_field_type', true);
            $value = '';

            foreach ($form_data as $data) {
                if ($data['name'] == $field_name) {
                    $value = $data['value'];
                    break;
                }
            }

            $is_dependent = get_post_meta($field_id, 'af_ch_f_dependable', true);

            if ('yes' == $is_dependent) {
                continue;
            }

            if ('fileupload' == $_type) {
                continue;
            }

            if (in_array($_type, ['message', 'heading'])) {
                continue;
            }

            if ('googlecaptcha' == $_type) {
                continue;
            }

            if (empty($value) && ('on' == $_required || 'privacy' == $_type)) {
                $errors[] = get_the_title($field_id) . ' is required';
            }

            if ('email' == $_type && !is_email($value)) {
                $errors[] = get_the_title($field_id) . ' is a not a valid email address';
            }

            if ('number' == $_type && !is_numeric($value)) {
                $errors[] = get_the_title($field_id) . ' is a not a valid number';
            }
        }

        if (!empty($errors)) {
            wp_send_json_error($errors);
        } else {
            foreach ($form_data as $field) {
                delete_user_meta($user_id, $field['name']);
                update_user_meta($user_id, $field['name'], $field['value']);
            }

            wp_send_json_success();
        }
    }
}
