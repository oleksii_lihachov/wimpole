<?php

class UsersExport
{
    public function __construct()
    {
        add_action('admin_menu', [$this, 'add'], 25);
        add_action('admin_action_user_export', [$this, 'handler']);
    }

    public function add()
    {
        add_submenu_page(
            'tools.php',
            'Users export',
            'Users export',
            'manage_options',
            'users_export',
            [$this, 'display'],
            20
        );
    }

    public function display()
    {
        ?>
        <div class="wrap"><h2>Users exporting</h2></div>
        <form method="POST" action="<?php echo admin_url('admin.php'); ?>">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row"><label for="users_role">Role</label></th>
                    <td>
					    <select name="role" id="users_role">
                            <option value="">Every Role</option>
						    <?php
                            global $wp_roles;
        foreach ($wp_roles->role_names as $role => $name) {
            echo "\n\t<option value='" . esc_attr($role) . "'>$name</option>";
        }
        ?>
					    </select>
				    </td>
                </tr>
                <tr valign="top">
				    <th scope="row"><label>Date range</label></th>
				<td>
					
				</td>
			</tr>
            </table>

            <input type="hidden" name="action" value="user_export" />
            <input type="submit" value="Export" />
        </form>
        <?php
    }

    public function mapping()
    {
        return [
            'afreg_additional_5925' => 'what_is_your_profession',
            'afreg_additional_5927' => 'registration_number_if_available_gmc_gdc_gphc',
            'afreg_additional_9614' => 'do_you_want_to_register_as_a_prescriber',
            'afreg_additional_6449' => 'reg_date_of_birth',
            'afreg_additional_5944' => 'i_confirm_that_i_hold_the_necessary_qualification',
            'afreg_additional_6448' => 'current_please_upload_your_id_passport_required',
            'afreg_additional_5926' => 'current_please_upload_your_training_certificate_required',
            // 'afreg_additional_6811' => 'not found',
            // 'afreg_additional_6812' => 'not found',
            // 'afreg_additional_6813' => 'not found',
            // 'afreg_additional_6814' => 'not found',
            // 'afreg_additional_6815' => 'not found',
            // 'afreg_additional_38541' => 'not found',
            'afreg_additional_19212' => 'current_in_order_to_verify_your_id_please_upload_a_recent',
            'afreg_additional_7247' => 'please_upload_your_toxins_training_certificate',
            'afreg_additional_9612' => 'current_please_upload_extra_training_certificate_or_insurance',
            'af_ch_f_8902' => 'prescriber_registered_email',
            'af_ch_f_8440' => 'patient_first_name',
            'af_ch_f_8442' => 'patient_address',
            'af_ch_f_8441' => 'patient_last_name',
            'af_ch_f_8444' => 'patient_address_line_2_optional',
            'af_ch_f_8443' => 'patient_city',
            'af_ch_f_8445' => 'patient_post_code',
            'af_ch_f_8446' => 'patient_date_of_birth',
            'af_ch_f_9847' => 'patient_email',
            'af_ch_f_8447' => 'patient_phone',
        ];
    }

    public function handler()
    {
        $key_mapping = $this->mapping();

        $args = [
            'fields' => 'all_with_meta',
            'role' => stripslashes($_POST['role'])
        ];
        $users = get_users($args);

        if (!$users) {
            $referer = add_query_arg('error', 'empty', wp_get_referer());
            wp_redirect($referer);
            exit;
        }

        $sitename = sanitize_key(get_bloginfo('name'));
        if (!empty($sitename)) {
            $sitename .= '.';
        }
        $filename = $sitename . 'users.' . date('Y-m-d-H-i-s') . '.csv';

        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Content-Type: text/csv; charset=' . get_option('blog_charset'), true);

        $fields = [
            'ID', 'user_nicename', 'user_email', 'user_registered', 'display_name', 'first_name', 'last_name', 'description', 'last_update',
            'billing_state', 'shipping_first_name', 'shipping_last_name', 'shipping_company', 'shipping_address_1',
            'shipping_address_2', 'shipping_city', 'shipping_postcode', 'shipping_country',
            'shipping_state', 'shipping_method', 'paying_customer', 'billing_first_name',
            'billing_last_name', 'billing_company', 'billing_address_1', 'billing_city', 'billing_postcode',
            'billing_country', 'billing_email', 'billing_phone', '_order_count',
            '_last_order', 'wcpay_currency', 'billing_address_2', 'shipping_phone', 'birth_date', 'date_of_birth',
            'account_status', 'afreg_new_user_status', 'afreg_additional_5925', 'afreg_additional_5926',
            'afreg_additional_5927', 'afreg_additional_5944', 'patient_name', 'patient_address',
            'patient_postcode', 'telephone', 'afreg_additional_6449', 'afreg_additional_6448',
            // 'afreg_additional_6811', 'afreg_additional_6812', 'afreg_additional_6813', 'afreg_additional_6814', 'afreg_additional_6815', afreg_additional_38541,
            'billing_patient_details', 'billing_patient_email', 'billing_does_your_patient_have_a_history_of_severe_allergy/anaphylaxis?',
            'billing_is_your_patient_currently_receiving_any_medical_treatment?', 'billing_if_yes,_please_give_more_detail:',
            'billing_has_your_patient_previously_received_any_aesthetic_treatments', 'billing_if_yes,_please_give_more_detail_2',
            'billing_has_your_patient_had_any_treatment_with_dermal_fillers,_absorbable_dermal_fillers,_semi-_permanent_dermal_fillers_or_botulinum_toxin?',
            'billing_if_yes,_please_give_more_detail_3', 'billing_is_your_patient_currently_taking_any_steroid,_aspirin_or_anticoagulant?',
            'billing_does_your_patient_suffer_from_any_allergies,_in_particular_allergies_to_hyaluronic_acid,_anaesthetics,__or_lidocaine?',
            'billing_does_your_patient_suffer_from_untreated_epilepsy?', 'billing_does_your_patient_suffer_from_cardiac_conduction_disorders?',
            'billing_patient_name', 'billing_telephone', 'billing_is-your-patient-pregnant-or-breast-feeding',
            'billing_does_your_patient_have_any_skin_infection_or_inflammatory_problems',
            'billing_patient_address', 'billing_patient_postcode', 'billing_date_of_birth',
            'billing_is_your_patient_currently_receiving_any_medical_treatment', 'billing_if_yes,_please_give_more_detail_1',
            'billing_email2', 'billing_does_your_patient_have_a_history_of_severe_allergy',
            'billing_if_yes_please_give_more_detail_1', 'billing_if_yes_please_give_more_detail_2',
            'billing_has_your_patient_had_any_treatment_with_dermal_fillers', 'billing_if_yes_please_give_more_detail_3',
            'billing_is_your_patient_currently_taking_any_steroid', 'billing_does_your_patient_suffer_from_any_allergies',
            'billing_does_your_patient_suffer_from_untreated_epilepsy', 'billing_has_your_patient_previously_received_any_aesthetic_treatment',
            'afreg_additional_7247', '_money_spent', 'afreg_additional_9614', 'afreg_additional_9612',
            'afreg_additional_19212', 'expired_at', 'reviewed_by',
            'last_review_by', 'are_you_an_aesthetic_practitioner_or_beautician',
            '_are_you_an_aesthetic_practitioner_or_beautician', 'training_certificate', 'registration_number_gmc_gdc_gphc_etc_if_available',
            'af_ch_f_8902', 'af_ch_f_8440', 'af_ch_f_8442', 'af_ch_f_8441', 'af_ch_f_8444', 'af_ch_f_8443',
            'af_ch_f_8445', 'af_ch_f_8446', 'af_ch_f_9847', 'af_ch_f_8447',            
        ];

        $headers = [];

        foreach ($fields as $field) {
            if (in_array($field, array_keys($key_mapping))) {
                $headers[] = '"' . $key_mapping[$field] . '"';
            } else {
                $headers[] = '"' . strtolower($field) . '"';
            }
        }

        echo implode(',', $headers) . "\n";

        foreach ($users as $user) {
            $data = [];
            foreach ($fields as $field) {
                $value = isset($user->{$field}) ? $user->{$field} : '';
                $value = is_array($value) ? serialize($value) : $value;
                $data[] = '"' . str_replace('"', '""', $value) . '"';
            }
            echo implode(',', $data) . "\n";
        }

        exit;
    }
}
