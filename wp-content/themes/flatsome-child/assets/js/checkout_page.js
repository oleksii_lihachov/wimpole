const $ = jQuery;
    const wc_checkout_coupons = {
        init: function() {
            $( document.body ).on( 'click', 'a.showcoupon-second', this.show_coupon_form );
            $( document.body ).on( 'click', '.woocommerce-remove-coupon', this.remove_coupon );
            $( 'form.checkout_coupon' ).hide().on( 'submit', this.submit );
        },
        show_coupon_form: function() {
            $( '.checkout_coupon_second' ).slideToggle( 400, function() {
                $( '.checkout_coupon_second' ).find( ':input:eq(0)' ).trigger( 'focus' );
            });
            return false;
        },
        submit: function() {
            var $form = $( this );

            if ( $form.is( '.processing' ) ) {
                return false;
            }

            $form.addClass( 'processing' ).block({
                message: null,
                overlayCSS: {
                    background: '#fff',
                    opacity: 0.6
                }
            });

            var data = {
                security:		wc_checkout_params.apply_coupon_nonce,
                coupon_code:	$form.find( 'input[name="coupon_code"]' ).val()
            };

            $.ajax({
                type:		'POST',
                url:		wc_checkout_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'apply_coupon' ),
                data:		data,
                success:	function( code ) {
                    $( '.woocommerce-error, .woocommerce-message' ).remove();
                    $form.removeClass( 'processing' ).unblock();

                    if ( code ) {
                        $form.before( code );
                        $form.slideUp();

                        $( document.body ).trigger( 'applied_coupon_in_checkout', [ data.coupon_code ] );
                        $( document.body ).trigger( 'update_checkout', { update_shipping_method: false } );
                    }
                },
                dataType: 'html'
            });

            return false;
        },
        remove_coupon: function( e ) {
            e.preventDefault();

            var container = $( this ).parents( '.woocommerce-checkout-review-order' ),
                coupon    = $( this ).data( 'coupon' );

            container.addClass( 'processing' ).block({
                message: null,
                overlayCSS: {
                    background: '#fff',
                    opacity: 0.6
                }
            });

            var data = {
                security: wc_checkout_params.remove_coupon_nonce,
                coupon:   coupon
            };

            $.ajax({
                type:    'POST',
                url:     wc_checkout_params.wc_ajax_url.toString().replace( '%%endpoint%%', 'remove_coupon' ),
                data:    data,
                success: function( code ) {
                    $( '.woocommerce-error, .woocommerce-message' ).remove();
                    container.removeClass( 'processing' ).unblock();

                    if ( code ) {
                        $( 'form.woocommerce-checkout' ).before( code );

                        $( document.body ).trigger( 'removed_coupon_in_checkout', [ data.coupon_code ] );
                        $( document.body ).trigger( 'update_checkout', { update_shipping_method: false } );

                        // Remove coupon code from coupon field
                        $( 'form.checkout_coupon_second' ).find( 'input[name="coupon_code"]' ).val( '' );
                    }
                },
                error: function ( jqXHR ) {
                    if ( wc_checkout_params.debug_mode ) {
                        /* jshint devel: true */
                        console.log( jqXHR.responseText );
                    }
                },
                dataType: 'html'
            });
        }
    };


    wc_checkout_coupons.init();

//     jQuery('.billing_radio-btn').each(function () {
//         jQuery(this).find('input').each(function () {
//             if(jQuery(this)[0].value === "no") {
//                 jQuery(this).attr('checked', true);
//             }
//         });
//     })
//
//     jQuery('.billing_checkbox-btn').each(function () {
//         jQuery(this).find('input[type="radio"]').attr('checked', true);
//     })
