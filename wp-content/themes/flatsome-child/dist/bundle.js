/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/js/fix-login-modal.js":
/*!**************************************!*\
  !*** ./assets/js/fix-login-modal.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// When user press add to cart modal will not triggered
// So we manually re-run code snippet from /assets/flatsome.js
jQuery(document).ready(function () {
  jQuery(document).on("added_to_cart", function () {
    jQuery("[data-open]", document).on("click", function (t) {
      t.stopPropagation();
      var e = jQuery(t.currentTarget),
          i = e.data("open"),
          n = e.data("color"),
          o = e.data("bg"),
          r = e.data("pos"),
          s = e.data("visible-after"),
          a = e.data("class"),
          l = e.attr("data-focus");
      e.offset();
      e.addClass("current-lightbox-clicked"), jQuery.magnificPopup.open({
        items: {
          src: i,
          type: "inline",
          tLoading: '<div class="loading-spin dark"></div>'
        },
        removalDelay: 300,
        closeBtnInside: flatsomeVars.lightbox.close_btn_inside,
        closeMarkup: flatsomeVars.lightbox.close_markup,
        focus: l,
        callbacks: {
          beforeOpen: function beforeOpen() {
            this.st.mainClass = "off-canvas ".concat(n, " off-canvas-").concat(r);
          },
          open: function open() {
            jQuery("html").addClass("has-off-canvas"), jQuery("html").addClass("has-off-canvas-" + r), a && jQuery(".mfp-content").addClass(a), o && jQuery(".mfp-bg").addClass(o), jQuery(".mfp-content .resize-select").change(), jQuery.fn.packery && jQuery("[data-packery-options], .has-packery").packery("layout");
          },
          beforeClose: function beforeClose() {
            jQuery("html").removeClass("has-off-canvas");
          },
          afterClose: function afterClose() {
            jQuery("html").removeClass("has-off-canvas-" + r), jQuery(".current-lightbox-clicked").removeClass("current-lightbox-clicked"), s && jQuery(i).removeClass("mfp-hide");
          }
        }
      }), t.preventDefault();
    });
  });
});

/***/ }),

/***/ "./assets/js/index.js":
/*!****************************!*\
  !*** ./assets/js/index.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _scss_style_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../scss/style.scss */ "./assets/scss/style.scss");
/* harmony import */ var _scss_style_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_scss_style_scss__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _fix_login_modal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fix-login-modal */ "./assets/js/fix-login-modal.js");
/* harmony import */ var _fix_login_modal__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_fix_login_modal__WEBPACK_IMPORTED_MODULE_1__);
 // import './checkout_page';



/***/ }),

/***/ "./assets/scss/style.scss":
/*!********************************!*\
  !*** ./assets/scss/style.scss ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

/******/ });
//# sourceMappingURL=bundle.js.map