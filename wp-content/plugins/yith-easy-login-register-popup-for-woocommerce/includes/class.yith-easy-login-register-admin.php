<?php
/**
 * Admin class
 *
 * @author  YITH
 * @package YITH Easy Login & Register Popup For WooCommerce
 * @version 1.0.0
 */

defined( 'YITH_WELRP' ) || exit; // Exit if accessed directly.

if ( ! class_exists( 'YITH_Easy_Login_Register_Admin' ) ) {
	/**
	 * YITH Easy Login & Register Popup For WooCommerce
	 *
	 * @since 1.0.0
	 */
	class YITH_Easy_Login_Register_Admin {

		/**
		 * Panel Object
		 *
		 * @var YIT_Plugin_Panel_WooCommerce|null
		 */
		protected $panel = null;

		/**
		 * Plugin panel page
		 *
		 * @var string
		 */
		protected $panel_page = 'yith_welrp_panel';

		/**
		 * Constructor
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function __construct() {
			// Add panel options.
			add_action( 'admin_menu', array( $this, 'register_panel' ), 5 );
			// Add action links.
			add_filter( 'plugin_action_links_' . plugin_basename( YITH_WELRP_PATH . '/' . basename( YITH_WELRP_FILE ) ), array( $this, 'action_links' ) );
			add_filter( 'yith_show_plugin_row_meta', array( $this, 'plugin_row_meta' ), 10, 5 );
			// Enqueue custom CSS.
			add_action( 'admin_enqueue_scripts', array( $this, 'custom_css' ), 99 );
		}

		/**
		 * Add a panel under YITH Plugins tab
		 *
		 * @since    1.0.0
		 * @author   Francesco Licandro <francesco.licandro@yithemes.com>
		 * @use      /Yit_Plugin_Panel class
		 * @return   void
		 * @see      plugin-fw/lib/yit-plugin-panel.php
		 */
		public function register_panel() {

			if ( ! empty( $this->panel ) ) {
				return;
			}

			$admin_tabs = array(
				'general'          => __( 'General Settings', 'yith-easy-login-register-popup-for-woocommerce' ),
				'first-step'       => __( 'First Step Options', 'yith-easy-login-register-popup-for-woocommerce' ),
				'login'            => __( 'Login Options', 'yith-easy-login-register-popup-for-woocommerce' ),
				'register'         => __( 'Register Options', 'yith-easy-login-register-popup-for-woocommerce' ),
				'lost-password'    => __( 'Lost Password Options', 'yith-easy-login-register-popup-for-woocommerce' ),
				'additional-popup' => __( 'Additional Popup', 'yith-easy-login-register-popup-for-woocommerce' ),
			);

			$args = array(
				'create_menu_page'   => true,
				'parent_slug'        => '',
				'page_title'         => 'YITH Easy Login & Register Popup for WooCommerce',
				'plugin_description' => _x( 'Makes the login, registration and password reset processes easier during the checkout and reduces the cart abandonment rate.', 'plugin description on options page', 'yith-easy-login-register-popup-for-woocommerce' ),
				'menu_title'         => 'Easy Login Register Popup',
				'capability'         => 'manage_options',
				'parent'             => '',
				'parent_page'        => 'yith_plugin_panel',
				'page'               => $this->panel_page,
				'admin-tabs'         => $admin_tabs,
				'options-path'       => YITH_WELRP_PATH . '/plugin-options',
				'class'              => yith_set_wrapper_class(),
				'help_tab'           => array(
					'main_video' => array(
						'desc' => _x( 'Check this video to learn how to <b>integrate Google and Facebook logins:</b>', '[HELP TAB] Video title', 'yith-easy-login-register-popup-for-woocommerce' ),
						'url'  => array(
							'en' => 'https://www.youtube.com/embed/cCPUjMeYpts',
							'it' => 'https://www.youtube.com/embed/egvOA54JpMM',
							'es' => 'https://www.youtube.com/embed/h-vk_eeqcXU',
						),
					),
					'playlists'  => array(
						'en' => 'https://www.youtube.com/watch?v=cCPUjMeYpts&list=PLDriKG-6905lLtK7X-xTqQIVThY69A2pz',
						'it' => 'https://www.youtube.com/watch?v=egvOA54JpMM&list=PL9c19edGMs0-3057GdT-1hDdne3P_SCQi',
						'es' => 'https://www.youtube.com/watch?v=h-vk_eeqcXU&list=PL9Ka3j92PYJMrufP9BP16W2cGGegpoC7z',
					),
					'hc_url'     => 'https://support.yithemes.com/hc/en-us/categories/4698314455185-YITH-EASY-LOGIN-REGISTER-POPUP-FOR-WOOCOMMERCE',
					'doc_url'    => 'https://docs.yithemes.com/yith-easy-login-register-popup-for-woocommerce/',
				),
			);

			if ( ! class_exists( 'YIT_Plugin_Panel_WooCommerce' ) ) {
				require_once 'plugin-fw/lib/yit-plugin-panel-wc.php';
			}

			$this->panel = new YIT_Plugin_Panel_WooCommerce( $args );
		}

		/**
		 * Custom admin css plugin
		 *
		 * @since   1.0.0
		 * @author  Francesco Licandro <francesco.licandro@yithemes.com>
		 */
		public function custom_css() {
			if ( isset( $_GET['page'] ) && sanitize_text_field( wp_unslash( $_GET['page'] ) ) === $this->panel_page ) { // phpcs:ignore WordPress.Security.NonceVerification
				$custom_css = '#yith_welrp_additional_popup_options-description p{background:none!important;font-weight:400!important;width:auto!important;padding:0 10px 0 0!important;}
                #yith_welrp_additional_popup_options-description p:before{display:none!important;}';
				wp_add_inline_style( 'yith-plugin-fw-fields', $custom_css );
			}
		}

		/**
		 * Action Links: add the action links to plugin admin page
		 *
		 * @since    1.0.0
		 * @author   Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param array $links Links plugin array.
		 *
		 * @return   mixed
		 * @use      plugin_action_links_{$plugin_file_name}
		 */
		public function action_links( $links ) {
			$links = yith_add_action_links( $links, $this->panel_page, true, YITH_WELRP_SLUG );
			return $links;
		}

		/**
		 * Add the action links to plugin admin page
		 *
		 * @since    1.0.0
		 * @author   Francesco Licandro <francesco.licandro@yithemes.com>
		 * @use      plugin_row_meta
		 * @param array    $new_row_meta_args An array of plugin row meta.
		 * @param string[] $plugin_meta       An array of the plugin's metadata,
		 *                                    including the version, author,
		 *                                    author URI, and plugin URI.
		 * @param string   $plugin_file       Path to the plugin file relative to the plugins directory.
		 * @param array    $plugin_data       An array of plugin data.
		 * @param string   $status            Status of the plugin. Defaults are 'All', 'Active',
		 *                                    'Inactive', 'Recently Activated', 'Upgrade', 'Must-Use',
		 *                                    'Drop-ins', 'Search', 'Paused'.
		 * @return   array
		 */
		public function plugin_row_meta( $new_row_meta_args, $plugin_meta, $plugin_file, $plugin_data, $status ) {
			if ( defined( 'YITH_WELRP_INIT' ) && YITH_WELRP_INIT === $plugin_file ) {
				$new_row_meta_args['slug']       = YITH_WELRP_SLUG;
				$new_row_meta_args['is_premium'] = true;
			}

			return $new_row_meta_args;
		}
	}
}
