<?php
/**
 * Popup handler class
 *
 * @author  YITH
 * @package YITH Easy Login & Register Popup For WooCommerce
 * @version 1.0.0
 */

defined( 'YITH_WELRP' ) || exit; // Exit if accessed directly.

if ( ! class_exists( 'YITH_Easy_Login_Register_Popup_Handler' ) ) {
	/**
	 * YITH Easy Login & Register Popup For WooCommerce
	 *
	 * @since 1.0.0
	 */
	class YITH_Easy_Login_Register_Popup_Handler {

		/**
		 * Ajax popup action
		 *
		 * @since 1.0.0
		 * @var string
		 */
		protected $form_action = 'yith_welrp_form_action';

		/**
		 * Check login ajax action
		 *
		 * @since 1.0.0
		 * @var string
		 */
		protected $check_login = 'yith_welrp_check_login';

		/**
		 * Current processed user
		 *
		 * @since 1.0.0
		 * @var WP_User|boolean
		 */
		protected $user = false;

		/**
		 * The reCaptcha public key
		 *
		 * @since 1.0.0
		 * @var string
		 */
		private $recaptcha_public = '';

		/**
		 * The reCaptcha private key
		 *
		 * @since 1.0.0
		 * @var string
		 */
		private $recaptcha_private = '';

		/**
		 * Constructor
		 *
		 * @since 1.0.0
		 * @return void
		 */
		public function __construct() {
			// Handle Ajax.
			add_action( 'wc_ajax_' . $this->form_action, array( $this, 'handle_form_action' ) );
			add_action( 'wp_ajax_nopriv_' . $this->form_action, array( $this, 'handle_form_action' ) );
			add_action( 'wc_ajax_' . $this->check_login, array( $this, 'check_login' ) );
			add_action( 'wp_ajax_nopriv_' . $this->check_login, array( $this, 'check_login' ) );
			add_filter( 'yith_welrp_script_data', array( $this, 'add_ajax_handler_data' ), 10, 1 );
			// Handle reCaptcha.
			if ( $this->enabled_recaptcha() ) {
				add_filter( 'yith_welrp_script_data', array( $this, 'add_recaptcha_key' ), 10, 1 );
				add_action( 'yith_welrp_before_register_action', array( $this, 'verify_captcha' ), 10, 1 );
			}
		}

		/**
		 * Add AJAX data to script data array
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro
		 * @param array $data Current script data.
		 * @return array
		 */
		public function add_ajax_handler_data( $data ) {
			$data = array_merge(
				$data,
				array(
					'formAction' => $this->form_action,
					'checkLogin' => $this->check_login,
				)
			);

			return $data;
		}

		/**
		 * Check for login
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @return void
		 */
		public function check_login() {
			wp_send_json_success(
				array(
					'logged' => is_user_logged_in(),
				)
			);
		}

		/**
		 * Handle form action
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @return void
		 * @throws Exception Handle form errors.
		 */
		public function handle_form_action() {

			try {

				$response = array( 'message' => '' );
				$action   = ! empty( $_POST['action'] ) ? preg_replace( '/[^a-z_-]/', '', sanitize_text_field( wp_unslash( $_POST['action'] ) ) ) : 'first-step'; // phpcs:ignore WordPress.Security.NonceVerification
				$handler  = 'handle_' . str_replace( '-', '_', $action );

				if ( empty( $_POST['user_login'] ) || ! method_exists( $this, $handler ) ) { // phpcs:ignore WordPress.Security.NonceVerification
					wp_die( - 1, 400 ); // Malformed request.
				}

				$user_login = wc_clean( $_POST['user_login'] ); // phpcs:ignore WordPress.Security.NonceVerification, WordPress.Security.ValidatedSanitizedInput.MissingUnslash, WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
				// Try to set user from posted data.
				$this->try_set_user_from_posted( $user_login, $action );
				// Then validate current user for action.
				$this->validate_user_action( $action, $user_login );
				// At the end handle action.
				// Redefining user_login ensures we return the right case.
				$user_login = $this->user ? $this->user->user_login : $user_login;
				$response   = array_merge( $response, $this->$handler( $user_login ) );

				wp_send_json_success( apply_filters( 'yith_welrp_ajax_form_action_response', $response, $action, $user_login ) );

			} catch ( Exception $e ) {
				$message = $e->getMessage();
				// If empty get the standard message.
				if ( ! $message ) {
					$message = yith_welrp_get_std_error_message();
				}

				wp_send_json_error(
					array(
						'message' => $message,
					)
				);
			}
		}

		/**
		 * Get an user login from posted data
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param string $user_login The user login.
		 * @param string $action Current action.
		 * @return void
		 */
		protected function try_set_user_from_posted( $user_login, $action ) {
			if ( is_email( $user_login ) ) {
				$this->user = get_user_by( 'email', $user_login );
			} elseif ( 'yes' === get_option( 'yith_welrp_allow_username', 'no' ) || 'first-step' !== $action ) {
				$this->user = get_user_by( 'login', sanitize_user( $user_login ) );
			}
		}

		/**
		 * Validate an user action based on current user
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param string $action Current action.
		 * @param string $user_login The user login.
		 * @return void
		 * @throws Exception Validate user errors.
		 */
		protected function validate_user_action( $action, $user_login ) {

			$username_enabled = 'yes' === get_option( 'yith_welrp_allow_username', 'no' );

			if ( ( 'register' === $action && $this->user )
			     || ( ! $this->user && ( 'authenticate-lost-password' === $action || 'set-new-password' === $action ) ) ) {
				// Malformed request, send 400. This case should never happen.
				wp_die( - 1, 400 );
			} elseif ( ( 'first-step' === $action && ! $username_enabled && ! is_email( $user_login ) ) ||
			           ( ( 'login' === $action || 'lost-password' === $action ) && ! $this->user ) ) {
				// In this case, because the user doesn't exists, user must add a valid email/username.
				$message = $username_enabled ? __( 'Please enter a valid username or email address.', 'yith-easy-login-register-popup-for-woocommerce' )
					: __( 'Please enter a valid email address.', 'yith-easy-login-register-popup-for-woocommerce' );
				throw new Exception( $message );
			}
		}

		/**
		 * Handle request action. User add his email/username and ask to register/login
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param string $user_login The user login (email|username).
		 * @return array
		 * @throws Exception First step error.
		 */
		protected function handle_first_step( $user_login ) {

			$action = $this->user ? 'login' : 'register';
			// Start build a response.
			$response = array(
				'popup' => array(
					'user_login' => $user_login,
				),
			);

			// Filter avatar url.
			add_filter( 'get_avatar_url', array( $this, 'default_avatar_url' ), 10, 3 );

			$default_wp_avatar = get_option( 'avatar_default' );

			if ( 'login' === $action ) {
				$response['action'] = array( 'nextSection' => 'login-section' );
				$response['popup']  = array_merge(
					$response['popup'],
					array(
						'title'      => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_login_title', _x( 'Welcome back, [username]!', '[username] is a placeholder for username.', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
						'message'    => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_login_message', __( 'Great to see you again! Enter your password to continue.', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
						'avatar'     => get_avatar( $this->user->user_email, 55, $default_wp_avatar, '' ),
						'user_email' => $this->user->user_email,
					)
				);
			} else {

				$button_label = empty( $_POST['additional'] ) // phpcs:ignore WordPress.Security.NonceVerification.Missing
					? get_option( 'yith_welrp_popup_register_button_label', __( 'Register and proceed to checkout', 'yith-easy-login-register-popup-for-woocommerce' ) )
					: get_option( 'yith_welrp_additional_popup_register_button', __( 'Register', 'yith-easy-login-register-popup-for-woocommerce' ) );

				$response['action'] = array( 'nextSection' => 'register-section' );
				$response['popup']  = array_merge(
					$response['popup'],
					array(
						'title'        => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_register_title', __( 'You are new here. Create your account!', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
						'message'      => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_register_message', __( 'It seems you don\'t have an account on [blogname] yet. But don\'t worry, you can create one and then complete your order.', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
						'button_label' => $button_label,
						'avatar'       => get_avatar( 0, 55, 'mystery', '', array( 'force_default' => true ) ),
						'email_field'  => ! is_email( $user_login ),
					)
				);
			}

			return $response;
		}

		/**
		 * Handle login action. User is already register and want to login
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param string $user_login The user login (email|username).
		 * @return array
		 * @throws Exception Handle login errors.
		 */
		protected function handle_login( $user_login ) {

			if ( empty( $_POST['user_password'] ) ) { // phpcs:ignore WordPress.Security.NonceVerification
				throw new Exception( __( 'Password field cannot be empty.', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}

			do_action( 'yith_welrp_before_login_action', $this->user );

			// Avoid Advanced noCaptcha & invisible Captcha check.
			add_filter( 'anr_verify_captcha_pre', '__return_true' );

			// Perform the login.
			$user = wp_signon(
				apply_filters(
					'yith_welrp_login_params',
					array(
						'user_login'    => $user_login,
						'user_password' => $_POST['user_password'], // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash, WordPress.Security.NonceVerification
						'remember'      => ! empty( $_POST['remeberme'] ), // phpcs:ignore WordPress.Security.NonceVerification
					)
				),
				is_ssl()
			);

			if ( is_wp_error( $user ) ) {
				$message = $user->get_error_message();
				if ( 'incorrect_password' === $user->get_error_code() ) {
					$message = preg_replace( '/(<a).*(\/a>)/', '', $message );
				}

				throw new Exception( $message );
			}

			do_action( 'yith_welrp_after_login_action', $user );

			// Because you are going to redirect user, use wc notice to send a message.
			wc_add_notice( __( 'You have successfully logged in, great to see you again!', 'yith-easy-login-register-popup-for-woocommerce' ) );

			return array( 'action' => array( 'redirectTo' => yith_welrp_get_redirect_url_from_posted() ) );
		}

		/**
		 * Handle register action. User is new and want to register
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param string $user_login The user login (email|username).
		 * @return array
		 * @throws Exception Handle register errors.
		 */
		protected function handle_register( $user_login ) {

			// In this case user_login must be an email.
			$username = ! is_email( $user_login ) ? $user_login : wc_create_new_customer_username( $user_login );
			$email    = isset( $_POST['reg_email'] ) ? wc_clean( $_POST['reg_email'] ) : $user_login; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash, WordPress.Security.NonceVerification
			$password = isset( $_POST['reg_password'] ) ? $_POST['reg_password'] : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash, WordPress.Security.NonceVerification

			if ( ! is_email( $email ) ) {
				throw new Exception( __( 'Please enter a valid email address.', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}
			$user = get_user_by( 'email', $email );
			if ( $user ) {
				throw new Exception( __( 'An account is already registered with this email.', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}
			if ( 'yes' === get_option( 'yith_welrp_popup_register_repeat_password', 'no' ) && ( empty( $_POST['reg_password_2'] ) || $password !== $_POST['reg_password_2'] ) ) { // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash, WordPress.Security.NonceVerification.Missing
				throw new Exception( __( 'Passwords do not match! Please try again.', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}
			if ( 'yes' === get_option( 'yith_welrp_popup_register_policy_enabled', 'no' ) && empty( $_POST['terms_policy'] ) ) { // phpcs:ignore WordPress.Security.NonceVerification.Missing
				throw new Exception( __( 'You must accept our terms and privacy policy!', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}

			do_action( 'yith_welrp_before_register_action', $user_login );

			// Avoid Advanced noCaptcha & invisible Captcha check.
			add_filter( 'anr_verify_captcha_pre', '__return_true' );

			$new_customer = wc_create_new_customer( $email, wc_clean( $username ), $password );
			if ( is_wp_error( $new_customer ) ) {
				throw new Exception( $new_customer->get_error_message() );
			}
			// Finally login.
			wc_set_customer_auth_cookie( $new_customer );

			do_action( 'yith_welrp_after_register_action', $user_login, $new_customer );

			// Because you are going to redirect user, use wc notice to send a message.
			wc_add_notice( __( 'Your account has been successfully created. Your login details have been sent to your email address.', 'yith-easy-login-register-popup-for-woocommerce' ) );

			return array( 'action' => array( 'redirectTo' => yith_welrp_get_redirect_url_from_posted() ) );
		}

		/**
		 * Handle lost password action. User is asking to reset his password
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param string $user_login The user login (email|username).
		 * @return array
		 * @throws Exception Handle lost password errors.
		 */
		protected function handle_lost_password( $user_login ) {

			do_action( 'yith_welrp_before_reset_password_action', $user_login );

			if ( ! $this->user || ( is_multisite() && ! is_user_member_of_blog( $this->user->ID, get_current_blog_id() ) ) ) {
				throw new Exception( __( 'Please enter a valid email address.', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}

			do_action( 'retrieve_password', $user_login );

			$allow = apply_filters( 'allow_password_reset', true, $this->user->ID );
			if ( ! $allow ) {
				throw new Exception( __( 'Password reset is not allowed for this user', 'yith-easy-login-register-popup-for-woocommerce' ) );
			} elseif ( is_wp_error( $allow ) ) {
				throw new Exception( $allow->get_error_message() );
			}

			$type = get_option( 'yith_welrp_popup_lost_password_recover_type', 'classic' );

			// Send email notification.
			WC()->mailer(); // Load email classes.

			$response = array(
				'action' => array( 'nextSection' => 'lost-password-section' ),
			);

			if ( 'with-code' === $type ) {

				$authentication_code = apply_filters( 'yith_welrp_authentication_code', wp_generate_password( 10, false, false ) );
				// Save as user meta with expired date.
				update_user_meta( $this->user->ID, '_yith_welrp_auth_code', $authentication_code );
				// Send mail.
				do_action( 'send_yith_welrp_customer_authentication_code_notification', $this->user, $authentication_code );

				$response['message'] = isset( $_POST['resend'] ) ? __( 'A new authentication code was sent successfully to your email', 'yith-easy-login-register-popup-for-woocommerce' ) : ''; // phpcs:ignore WordPress.Security.NonceVerification.Missing
				$response['popup']   = array(
					'action'       => 'authenticate-lost-password',
					'user_login'   => $user_login,
					'title'        => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_lost_password_authentication_title', __( 'Authentication required', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
					'button_label' => get_option( 'yith_welrp_popup_lost_password_authentication_button_label', __( 'Continue', 'yith-easy-login-register-popup-for-woocommerce' ) ),
					'message'      => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_lost_password_authentication_message', __( 'For your security we need to authenticate your request. We\'ve sent a code to your email. Please enter it below.', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
				);
			} else {
				// Get password reset key (function introduced in WordPress 4.4).
				$key = get_password_reset_key( $this->user );
				do_action( 'woocommerce_reset_password_notification', $user_login, $key );

				$response['message'] = isset( $_POST['resend'] ) ? __( 'A new password reset email has been sent to the email address of your account.', 'yith-easy-login-register-popup-for-woocommerce' ) : ''; // phpcs:ignore WordPress.Security.NonceVerification.Missing
				$response['popup']   = array(
					'action'     => 'lost-password-confirm',
					'user_login' => $user_login,
					'title'      => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_lost_password_confirm_title', __( 'Check your email', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
					'message'    => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_lost_password_confirm_message', __( 'A password reset email has been sent to the email address of your account. If you did not receive it, check your spam folder or click to receive another email.', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
				);
			}

			do_action( 'yith_welrp_after_reset_password_action', $user_login );

			return $response;
		}

		/**
		 * Handle authentication code for lost password
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param string $user_login The user login (email|username).
		 * @return array
		 * @throws Exception Handle auth lost password errors.
		 */
		protected function handle_authenticate_lost_password( $user_login ) {

			// Get code.
			$code         = ! empty( $_POST['authentication_code'] ) ? wc_clean( $_POST['authentication_code'] ) : ''; // phpcs:ignore WordPress.Security.NonceVerification, WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash
			$founded_user = $code ? get_users(
				array(
					'meta_key'     => '_yith_welrp_auth_code',
					'meta_value'   => $code,
					'meta_compare' => '=',
				)
			) : array();

			$founded_user = ! empty( $founded_user ) ? array_shift( $founded_user ) : '';

			if ( ! $code || ! $founded_user || $founded_user->user_login !== $user_login ) {
				throw new Exception( __( 'Invalid code.', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}

			$button_label = empty( $_POST['additional'] ) // phpcs:ignore WordPress.Security.NonceVerification.Missing
				? get_option( 'yith_welrp_popup_lost_password_set_button_label', __( 'Save password and proceed to checkout', 'yith-easy-login-register-popup-for-woocommerce' ) )
				: get_option( 'yith_welrp_additional_popup_set_password_button', __( 'Save password and access', 'yith-easy-login-register-popup-for-woocommerce' ) );

			$response = array(
				'action' => array( 'nextSection' => 'lost-password-section' ),
				'popup'  => array(
					'action'       => 'set-new-password',
					'user_login'   => $user_login,
					'reset_key'    => get_password_reset_key( $founded_user ),
					'title'        => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_lost_password_set_title', __( 'Set a new password and login', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
					'button_label' => $button_label,
					'message'      => yith_welrp_replace_placeholder( get_option( 'yith_welrp_popup_lost_password_set_message', __( 'Now you can set a new password for your account.', 'yith-easy-login-register-popup-for-woocommerce' ) ), $this->user ),
				),
			);

			return $response;
		}

		/**
		 * Handle set customer new password
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param string $user_login The user login (email|username).
		 * @return array
		 * @throws Exception Handle set new password errors.
		 */
		protected function handle_set_new_password( $user_login ) {

			// Validate user password reset key.
			$key  = isset( $_POST['reset_key'] ) ? wp_unslash( $_POST['reset_key'] ) : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.NonceVerification
			$user = check_password_reset_key( $key, $user_login );
			if ( is_wp_error( $user ) ) {
				throw new Exception( __( 'Reset key is invalid! Please reset your password again if needed.', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}

			// Get password.
			$password = isset( $_POST['new_password'] ) ? $_POST['new_password'] : ''; // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash, WordPress.Security.NonceVerification
			if ( 'yes' === get_option( 'yith_welrp_popup_lost_password_set_repeat_password', 'no' ) && ( empty( $_POST['new_password_2'] ) || $password !== $_POST['new_password_2'] ) ) { // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash, WordPress.Security.NonceVerification
				throw new Exception( __( 'Passwords do not match! Please try again.', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}

			wp_set_password( $password, $user->ID );
			// Delete user meta.
			delete_user_meta( $user->ID, '_yith_welrp_auth_code' );
			// Finally login.
			wc_set_customer_auth_cookie( $user->ID );

			// Because you are going to redirect user, use wc notice to send a message.
			wc_add_notice( __( 'The password of your account has been changed successfully', 'yith-easy-login-register-popup-for-woocommerce' ) );
			$response = array( 'action' => array( 'redirectTo' => yith_welrp_get_redirect_url_from_posted() ) );

			return $response;
		}

		/**
		 * Filter default avatar url to get the custom one
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro <francesco.licandro@yithemes.com>
		 * @param string $url The URL of the avatar.
		 * @param mixed  $id_or_email The Gravatar to retrieve. Accepts a user_id, gravatar md5 hash,
		 *                            user email, WP_User object, WP_Post object, or WP_Comment object.
		 * @param array  $args Arguments passed to get_avatar_data(), after processing.
		 * @return string
		 */
		public function default_avatar_url( $url, $id_or_email, $args ) {
			if ( $args['found_avatar'] || apply_filters( 'yith_welrp_use_wp_default_avatar', false ) ) {
				return $url;
			}

			return YITH_WELRP_ASSETS_URL . 'images/user.svg';
		}

		/**
		 * Check if reCaptcha is enabled
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro
		 * @return boolean
		 */
		public function enabled_recaptcha() {
			if ( 'yes' !== get_option( 'yith_welrp_popup_register_enable_recaptcha', 'no' ) ) {
				return false;
			}

			$this->recaptcha_public  = get_option( 'yith_welrp_popup_register_recaptcha_public_key', '' );
			$this->recaptcha_private = get_option( 'yith_welrp_popup_register_recaptcha_private_key', '' );

			if ( ! $this->recaptcha_public || ! $this->recaptcha_private ) {
				return false;
			}

			return true;
		}

		/**
		 * Add reCaptcha public key to script data
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro
		 * @param array $data The script data.
		 * @return array
		 */
		public function add_recaptcha_key( $data ) {
			$data['googleReCaptcha'] = $this->recaptcha_public;

			return $data;
		}

		/**
		 * Verify posted reCaptcha
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro
		 * @param string $user_login The user login (email|username).
		 * @return boolean
		 * @throws Exception Verify captcha error.
		 */
		public function verify_captcha( $user_login ) {
			if ( empty( $_REQUEST['g-recaptcha-response'] ) || ! $this->is_recaptcha_valid( $_REQUEST['g-recaptcha-response'] ) ) { // phpcs:ignore WordPress.Security.ValidatedSanitizedInput.InputNotSanitized, WordPress.Security.ValidatedSanitizedInput.MissingUnslash, WordPress.Security.NonceVerification
				throw new Exception( __( 'ReCaptcha validation error. Please try again.', 'yith-easy-login-register-popup-for-woocommerce' ) );
			}

			return true;
		}

		/**
		 * Validate reCaptcha
		 *
		 * @since  1.0.0
		 * @author Francesco Licandro
		 * @param string $response The reCaptcha response.
		 * @return boolean
		 */
		protected function is_recaptcha_valid( $response ) {
			if ( ! $response || ! $this->recaptcha_private ) {
				return false;
			}

			$ch = curl_init();
			curl_setopt( $ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify' );
			curl_setopt( $ch, CURLOPT_POST, 1 );
			curl_setopt(
				$ch,
				CURLOPT_POSTFIELDS,
				array(
					'secret'   => $this->recaptcha_private,
					'response' => $response,
				)
			);
			curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
			$output = curl_exec( $ch );

			curl_close( $ch );

			$output = json_decode( $output );

			return ! empty( $output->success ) ? $output->success : false;
		}
	}
}
