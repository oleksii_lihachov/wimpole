<?php

// TODO PHP7.x; declare(strict_type=1);
// TODO PHP7.x; type hints & return types
// TODO PHP7.1; constant visibility

namespace WPStaging\Pro\Backup\Task\Tasks\JobExport;

use Exception;
use WPStaging\Framework\Analytics\Actions\AnalyticsBackupCreate;
use WPStaging\Framework\Filesystem\FileObject;
use WPStaging\Framework\Filesystem\PathIdentifier;
use WPStaging\Framework\Queue\SeekableQueueInterface;
use WPStaging\Framework\Utils\Cache\Cache;
use WPStaging\Pro\Backup\Dto\StepsDto;
use WPStaging\Pro\Backup\Dto\TaskResponseDto;
use WPStaging\Pro\Backup\Dto\Task\Export\Response\CombineExportResponseDto;
use WPStaging\Pro\Backup\Entity\BackupMetadata;
use WPStaging\Pro\Backup\Entity\ListableBackup;
use WPStaging\Pro\Backup\Service\BackupMetadataEditor;
use WPStaging\Pro\Backup\Task\ExportTask;
use WPStaging\Vendor\Psr\Log\LoggerInterface;

class FinishBackupTask extends ExportTask
{
    /** @var PathIdentifier */
    protected $pathIdentifier;

    /** @var BackupMetadataEditor */
    protected $backupMetadataEditor;

    /** @var AnalyticsBackupCreate */
    protected $analyticsBackupCreate;

    const OPTION_LAST_BACKUP = 'wpstg_last_backup_info';

    public function __construct(LoggerInterface $logger, Cache $cache, StepsDto $stepsDto, SeekableQueueInterface $taskQueue, PathIdentifier $pathIdentifier, BackupMetadataEditor $backupMetadataEditor, AnalyticsBackupCreate $analyticsBackupCreate)
    {
        parent::__construct($logger, $cache, $stepsDto, $taskQueue);

        $this->pathIdentifier = $pathIdentifier;
        $this->backupMetadataEditor = $backupMetadataEditor;
        $this->analyticsBackupCreate = $analyticsBackupCreate;
    }

    public static function getTaskName()
    {
        return 'backup_export_finish';
    }

    public static function getTaskTitle()
    {
        return 'Finalizing Backup Export';
    }

    public function execute()
    {
        $exportFilePath = $this->jobDataDto->getBackupFilePath();

        // Store the "Size" of the Backup in the metadata, which is something we can only do after the backup is final.
        try {
            $this->signBackup($exportFilePath);
        } catch (Exception $e) {
            $this->logger->critical('The backup file could not be signed for consistency.');

            return $this->generateResponse();
        }

        // Validate the Backup
        if ($exportFilePath) {
            try {
                $this->validateBackup($exportFilePath);
            } catch (Exception $e) {
                $this->logger->critical('The backup file seems to be invalid.');

                return $this->generateResponse();
            }
        }

        $this->analyticsBackupCreate->enqueueFinishEvent($this->jobDataDto->getId(), $this->jobDataDto);

        $this->stepsDto->finish();

        $this->jobDataDto->setEndTime(time());

        update_option(static::OPTION_LAST_BACKUP, [
            'endTime' => time(), // Unix timestamp is timezone independent
            'duration' => $this->jobDataDto->getDuration(),
            'JobExportDataDto' => $this->jobDataDto,
        ], false);

        return $this->overrideGenerateResponse($this->makeListableBackup($exportFilePath));
    }

    /**
     * Signing the Backup aims to give it an identifier that can be checked for it's consistency.
     *
     * Currently, we use the size of the file. We can use this information later, during Restore or Upload,
     * to check if the Backup file we have is complete and matches the expected one.
     *
     * @param string $exportFilePath
     */
    protected function signBackup($exportFilePath)
    {
        clearstatcache();
        if (!is_file($exportFilePath)) {
            throw new \RuntimeException('The backup file is invalid.');
        }

        $file = new FileObject($exportFilePath, FileObject::MODE_APPEND_AND_READ);
        $backupMetadata = new BackupMetadata();
        $backupMetadata->hydrate($file->readBackupMetadata());

        /*
         * Before: "backupSize": ""
         * After:  "backupSize": 123456
         */
        $backupMetadata->setBackupSize($file->getSize() - 2 + strlen($file->getSize()));

        $this->backupMetadataEditor->setBackupMetadata($file, $backupMetadata);
    }

    /**
     * Check if the backup was successfully validated.
     *
     * @param string $exportFilePath
     */
    protected function validateBackup($exportFilePath)
    {
        clearstatcache();
        if (!is_file($exportFilePath)) {
            throw new \RuntimeException('The backup file is invalid.');
        }

        $file = new FileObject($exportFilePath);

        $backupMetadata = new BackupMetadata();
        $backupMetadata->hydrate($file->readBackupMetadata());

        if ($backupMetadata->getName() !== $this->jobDataDto->getName()) {
            throw new \RuntimeException('The backup file seems to be invalid (Unexpected Name in Metadata).');
        }

        if ($backupMetadata->getBackupSize() !== $file->getSize()) {
            throw new \RuntimeException('The backup file seems to be invalid (Unexpected Size in Metadata).');
        }

        $this->logger->info('The backup was validated successfully.');
    }

    /**
     * @param null|ListableBackup $backup
     *
     * @return CombineExportResponseDto|TaskResponseDto
     */
    private function overrideGenerateResponse(ListableBackup $backup = null)
    {
        add_filter('wpstg.task.response', function ($response) use ($backup) {
            if ($response instanceof CombineExportResponseDto) {
                $response->setBackupMd5($backup ? $backup->md5BaseName : null);
                $response->setBackupSize($backup ? size_format($backup->size) : null);
            }

            return $response;
        });

        return $this->generateResponse();
    }

    protected function getResponseDto()
    {
        return new CombineExportResponseDto();
    }

    /**
     * This is used to display the "Download Modal" after the backup completes.
     *
     * @see string src/Backend/public/js/wpstg-admin.js, search for "wpstg--backups--export"
     *
     * @param string $exportFilePath
     *
     * @return ListableBackup
     */
    protected function makeListableBackup($exportFilePath)
    {
        clearstatcache();
        $backup = new ListableBackup();
        $backup->md5BaseName = md5(basename($exportFilePath));
        $backup->size = filesize($exportFilePath);

        return $backup;
    }
}
