<?php

namespace WPStaging\Pro\Backup;

use WPStaging\Core\WPStaging;
use WPStaging\Framework\Security\Capabilities;
use WPStaging\Framework\Security\Nonce;
use WPStaging\Pro\Backup\BackgroundProcessing\Export\PrepareExport;
use WPStaging\Pro\Backup\Dto\Job\JobExportDataDto;
use WPStaging\Pro\Backup\Service\BackupsFinder;

use function WPStaging\functions\debug_log;

class BackupScheduler
{
    protected $backupsFinder;

    protected $processLock;

    public function __construct(BackupsFinder $backupsFinder, BackupProcessLock $processLock)
    {
        $this->backupsFinder = $backupsFinder;
        $this->processLock = $processLock;
    }

    const OPTION_BACKUP_SCHEDULES = 'wpstg_backup_schedules';

    public function getSchedules()
    {
        return get_option(static::OPTION_BACKUP_SCHEDULES, []);
    }

    public function maybeDeleteOldBackups(JobExportDataDto $jobExportDataDto)
    {
        $scheduleId = $jobExportDataDto->getScheduleId();

        // Not a scheduled backup, nothing to do.
        if (empty($scheduleId)) {
            return;
        }

        $schedules = get_option(static::OPTION_BACKUP_SCHEDULES, []);

        $schedule = array_filter($schedules, function ($schedule) use ($scheduleId) {
            return $schedule['scheduleId'] == $scheduleId;
        });

        if (empty($schedule)) {
            debug_log("Could not delete old backups for schedule ID $scheduleId as the schedule rotation plan was not found in the database.");

            return;
        }

        $schedule = array_shift($schedule);

        $rotation = absint($schedule['rotation']);

        $backupFiles = $this->backupsFinder->findBackupByScheduleId($scheduleId);

        // Early bail: Not enough backups to trigger the rotation
        if (count($backupFiles) < $rotation) {
            return;
        }

        // Sort backups, older first
        uasort($backupFiles, function ($backup1, $backup2) {
            /**
             * @var \SplFileInfo $backup1
             * @var \SplFileInfo $backup2
             */
            if ($backup1->getMTime() === $backup2->getMTime()) {
                return 0;
            }

            return $backup1->getMTime() < $backup2->getMTime() ? -1 : 1;
        });

        // Make sure array indexes are correctly ordered.
        $backupFiles = array_values($backupFiles);

        // Get exceeding backups, including an extra one for the backup that will be created right now.
        $backupFiles = array_slice($backupFiles, 0, max(1, count($backupFiles) - $rotation + 1));

        array_map(function ($file) {
            /**
             * @var \SplFileInfo $file
             */
            if (!unlink($file->getPathname())) {
                debug_log('Tried to cleanup old backups for backup plan rotation, but couldnt delete file: ' . $file->getPathname());
            }
        }, $backupFiles);
    }

    public function scheduleBackup(JobExportDataDto $jobExportDataDto, $scheduleId)
    {
        if (!isset(wp_get_schedules()[$jobExportDataDto->getScheduleRecurrence()])) {
            \WPStaging\functions\debug_log("Tried to schedule a backup, but schedule '" . $jobExportDataDto->getScheduleRecurrence() . "' is not registered as a WordPress cron schedule. Data DTO: " . wp_json_encode($jobExportDataDto));

            return;
        }

        $backupSchedule = [
            'scheduleId' => $scheduleId,
            'schedule' => $jobExportDataDto->getScheduleRecurrence(),
            'time' => $jobExportDataDto->getScheduleTime(),
            'name' => $jobExportDataDto->getName(),
            'rotation' => $jobExportDataDto->getScheduleRotation(),
            'isExportingPlugins' => $jobExportDataDto->getIsExportingPlugins(),
            'isExportingMuPlugins' => $jobExportDataDto->getIsExportingMuPlugins(),
            'isExportingThemes' => $jobExportDataDto->getIsExportingThemes(),
            'isExportingUploads' => $jobExportDataDto->getIsExportingUploads(),
            'isExportingOtherWpContentFiles' => $jobExportDataDto->getIsExportingOtherWpContentFiles(),
            'isExportingDatabase' => $jobExportDataDto->getIsExportingDatabase(),
        ];

        if (wp_next_scheduled('wpstg_create_cron_backup', [$backupSchedule])) {
            \WPStaging\functions\debug_log('[Schedule Backup Cron] Early bailed when registering the cron to create a backup on a schedule, because it already exists');

            return;
        }

        $this->registerScheduleInDb($backupSchedule);
        $this->reCreateCron();
    }

    /**
     * Registers a schedule in the Db.
     */
    protected function registerScheduleInDb($backupSchedule)
    {
        $backupSchedules = get_option(static::OPTION_BACKUP_SCHEDULES, []);
        if (!is_array($backupSchedules)) {
            $backupSchedules = [];
        }

        $backupSchedules[] = $backupSchedule;

        if (!update_option(static::OPTION_BACKUP_SCHEDULES, $backupSchedules, false)) {
            \WPStaging\functions\debug_log('[Schedule Backup Cron] Could not update BackupSchedules DB option');
        }
    }

    /**
     * AJAX callback that processes the backup schedule.
     *
     * @param $backupData
     */
    public function createCronBackup($backupData)
    {
        // Cron is hell to debug, so let's log everything that happens.
        $logId = wp_generate_password(4, false);

        \WPStaging\functions\debug_log("[Schedule Backup Cron - $logId] Received request to create a backup using Cron. Backup Data: " . wp_json_encode($backupData));

        try {
            \WPStaging\functions\debug_log("[Schedule Backup Cron - $logId] Preparing job");
            $jobId = WPStaging::make(PrepareExport::class)->prepare($backupData);
            \WPStaging\functions\debug_log("[Schedule Backup Cron - $logId] Successfully received a Job ID: $jobId");

            if ($jobId instanceof \WP_Error) {
                \WPStaging\functions\debug_log("[Schedule Backup Cron - $logId] Failed to create backup: " . $jobId->get_error_message());
            }
        } catch (\Exception $e) {
            \WPStaging\functions\debug_log("[Schedule Backup Cron - $logId] Exception thrown while preparing the Backup: " . $e->getMessage());
        }
    }

    /**
     * Ajax callback to dismiss a schedule.
     */
    public function dismissSchedule()
    {
        if (!current_user_can((new Capabilities())->manageWPSTG())) {
            return;
        }

        if (!(new Nonce())->requestHasValidNonce(Nonce::WPSTG_NONCE)) {
            return;
        }

        if (empty($_POST['scheduleId'])) {
            return;
        }

        try {
            $this->deleteSchedule($_POST['scheduleId']);
            wp_send_json_success();
        } catch (\Exception $e) {
            wp_send_json_error($e->getMessage());
        }
    }

    /**
     * Deletes a backup schedule.
     *
     * @param string $scheduleId The schedule ID to delete.
     */
    public function deleteSchedule($scheduleId)
    {
        $schedules = $this->getSchedules();

        $newSchedules = array_filter($schedules, function ($schedule) use ($scheduleId) {
            return $schedule['scheduleId'] != $scheduleId;
        });

        if (!update_option(static::OPTION_BACKUP_SCHEDULES, $newSchedules, false)) {
            \WPStaging\functions\debug_log('[Schedule Backup Cron] Could not update BackupSchedules DB option after removing schedule.');
            throw new \RuntimeException('Could not unschedule event from Db.');
        }

        $this->reCreateCron();
    }

    /**
     * @see OPTION_BACKUP_SCHEDULES The Db option that is the source of truth for Cron events.
     *                              The backup schedule cron events are deleted and re-created
     *                              based on what is in this db option.
     *
     *                              This way, we only care about preserving this option on Backup
     *                              Restore or Push, and we don't have to worry about re-scheduling
     *                              the Cron events or removing leftover schedules.
     */
    public function reCreateCron()
    {
        $schedules = $this->getSchedules();
        static::removeBackupSchedulesFromCron();

        foreach ($schedules as $schedule) {
            $timeToSchedule = new \DateTime('now', wp_timezone());
            if (is_array($schedule['time'])) {
                $hourAndMinute = $schedule['time'];
            } else {
                $hourAndMinute = explode(':', $schedule['time']);
            }

            // The next event should be scheduled later today or tomorrow? Compares "Hi (Hourminute)" timestamps to figure out.
            if ((int)sprintf('%s%s', $hourAndMinute[0], $hourAndMinute[1]) < (int)$timeToSchedule->format('Hi')) {
                $timeToSchedule->add(new \DateInterval('P1D'));
            }

            // Set hour and minute
            $timeToSchedule->setTime($hourAndMinute[0], $hourAndMinute[1]);

            /** @see \WPStaging\Pro\Backup\BackupServiceProvider::enqueueAjaxListeners */
            $result = wp_schedule_event($timeToSchedule->format('U'), $schedule['schedule'], 'wpstg_create_cron_backup', [$schedule]);

            // Early bail: Could not register Cron event.
            if (!$result || $result instanceof \WP_Error) {
                if ($result instanceof \WP_Error) {
                    $details = $result->get_error_message();
                } else {
                    $details = '';
                }

                \WPStaging\functions\debug_log('[Schedule Backup Cron] Failed to register the cron event. ' . $details);

                return;
            }
        }
    }

    /**
     * Removes all backup schedule events from WordPress Cron array.
     *
     * This is static so that it can be called from WP STAGING deactivation hook
     * without having to bootstrap the entire plugin.
     *
     * This is a low-level function that can run when WP STAGING has not been
     * bootstrapped, so there's no autoload nor Container available.
     */
    public static function removeBackupSchedulesFromCron()
    {
        $cron = get_option('cron');

        // Bail: Unexpected value - should never happen.
        if (!is_array($cron)) {
            return false;
        }

        // Remove any backup schedules from Cron
        foreach ($cron as $timestamp => &$events) {
            if (is_array($events)) {
                foreach ($events as $callback => &$args) {
                    if ($callback === 'wpstg_create_cron_backup') {
                        unset($cron[$timestamp][$callback]);
                    }
                }
            }
        }

        // After removing the backup schedule events,
        // we might have timestamps with no events.
        // So we remove any leftover timestamps that don't have any events.
        $cron = array_filter($cron, function ($timestamps) {
            return !empty($timestamps);
        });

        update_option('cron', $cron);

        return true;
    }

    /**
     * @return array An array where the first item is the timestamp, and the second is the backup callback.
     * @throws \Exception When there is no backup scheduled or one could not be found.
     */
    public function getNextBackupSchedule()
    {
        $cron = get_option('cron');

        // Bail: Unexpected value - should never happen.
        if (!is_array($cron)) {
            throw new \UnexpectedValueException();
        }

        ksort($cron, SORT_NUMERIC);

        // Remove any backup schedules from Cron
        foreach ($cron as $timestamp => &$events) {
            if (is_array($events)) {
                foreach ($events as $callback => &$args) {
                    if ($callback === 'wpstg_create_cron_backup') {
                        return [$timestamp, $cron[$timestamp][$callback]];
                    }
                }
            }
        }

        // No results found
        throw new \OutOfBoundsException();
    }
}
