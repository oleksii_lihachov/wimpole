<?php

// WP STAGING version number
if (!defined('WPSTGPRO_VERSION')) {
    define('WPSTGPRO_VERSION', '4.1.6');
}

// Compatible up to WordPress Version
if (!defined('WPSTG_COMPATIBLE')) {
    define('WPSTG_COMPATIBLE', '5.9.0');
}
