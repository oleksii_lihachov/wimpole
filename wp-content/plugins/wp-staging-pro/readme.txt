=== WP STAGING PRO - Backup Duplicator & Migration  ===

Author URL: https://wordpress.org/plugins/wp-staging
Plugin URL: https://wordpress.org/plugins/wp-staging
Contributors: ReneHermi, WP-Staging
Donate link: https://wp-staging.com/#pricing
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: backup, database backup, staging, duplication, clone
Requires at least: 3.6+
Tested up to: 5.8.2
Stable tag: 4.1.6
Requires PHP: 5.6

A backup & duplicator plugin - clone, move, duplicate & migrate websites to staging, backup, and development sites for authorized users only.

== Description ==

<h3>WP STAGING for WordPress Cloning & Migration</h3>
This duplicator, staging, and backup plugin can create an exact copy of your entire website in seconds.* Great for staging, backup, or development purposes.
(Cloning and Backup time depends on the size of your website)<br /><br />
This clone plugin creates a clone of your website into a subfolder or subdomain (Pro) of your main WordPress installation. The clone includes an entire copy of your database.
 <br /> <br />
<strong>Note: </strong> For pushing & migrating plugins and theme files to production site, check out the pro edition [https://wp-staging.com/](https://wp-staging.com/ "WP STAGING PRO")
<br /><br />
All the time-consumptive database and file cloning operations are done in the background. The plugin even automatically does an entire search & replacement of all serialized links and paths.
 <br /><br />
This staging and backup plugin can clone your website even if it runs on a weak shared hosting server.
 <br /><br />
 <br /><br />
WP STAGING can help you to prevent your website from being broken or unavailable because of installing untested plugin updates!

[youtube https://www.youtube.com/watch?v=vkv52s36Yvg]

= Main Features =

* WP STAGING clones the whole production site into a subfolder like example.com/staging-site.
* Easy to use! Create a clone / backup site by clicking one button
* No Software as a Service - No account needed! All data belong to you only and stay on your server.
* No server timeouts on huge websites or/and small hosting servers
* Very fast - Migration and clone / backup process takes only a few seconds or minutes, depending on the website's size and server I/O power.
* Use the clone as part of your backup strategy
* Only administrators can access the clone / backup website.
* SEO friendly: The clone website is unavailable to search engines due to a custom login prompt and the meta tag no-index.
* The admin bar on the staging / backup website is orange colored and shows clearly when you work on the staging site.
* Extensive logging features
* Supports all popular web servers: Apache, Nginx, and Microsoft IIS
* <strong>[Premium]: </strong>Choose a separate database and select a custom directory for cloning
* <strong>[Premium]: </strong>Make the clone website available from a subdomain like dev.example.com
* <strong>[Premium]: </strong>Push & migrate entire clone site inc. all plugins, themes, and media files to the production website.
* <strong>[Premium]: </strong>Define user roles that get access to the clone site only. For instance, clients or external developers.
* <strong>[Premium]: </strong>Migration and cloning of WordPress multisites
* <strong>[Premium]: </strong>Scheduled Backups running in the background
* <strong>[Premium]: </strong>Backup of WordPress multisites (Released soon)

> Note: Some features are Premium. You need WP STAGING | PRO to use those features. [Read More about WP STAGING | PRO](https://wp-staging.com)!

= Additional Features WP STAGING | PRO Edition  =

* Backup entire website, even with millions of database rows faster and less resource intensive than with other plugins
* Migrate and transfer WordPress to another host or domain
* Cloning and migration of WordPress multisite
* Define a separate database and a custom directory for cloning
* Clone your website into a subdomain
* Specify certain user roles for accessing the staging site
* Copy all modifications from the staging site to the production website

<strong>Change your workflow of updating themes and plugins:</strong>

1. Use WP STAGING to clone a production website for staging, testing, or backup purposes
2. Create a backup of your website
3. Customize the theme, configuration, update or install new plugins
4. Test everything on your staging site and keep a backup of the original site
5. If everything works on the staging site start the migration and copy all modifications to your production site!

<h3> Can´t I just use my local WordPress development system like xampp / lampp for testing and backup purposes? </h3>

You can test your website locally but if your local hardware and software environment is not a 100% exact clone of your production server there is NO guarantee that every aspect of your local copy is working on your production website exactly as you expect it.
There are some obvious things like differences in the config of PHP and the server you are running but even such non-obvious settings like the amount of RAM or the CPU performance can lead to unexpected results on your production website.
There are dozens of other possible cause of failure which can not be handled well when you are testing your changes on a local platform only without creating a backup staging site.

This is where WP STAGING comes into play... Site cloning, backup, and staging site creation simplified and with enterprise code quality on a new level.!


== Frequently Asked Questions ==

= Why should I use a Backup & Staging Website? =

Plugin updates and theme customizations should be tested on a staging / backup platform first before they are done on your production website.
It's recommended to have the staging / backup platform on the same server where the production website is located to use the same hardware and software environment for your test & backup website and to catch all possible errors during testing.

Before you update a plugin or going to install a new one, it is highly recommended to check out the modifications on a clone / backup of your production website.
That ensures that any modifications work on your production website without throwing unexpected errors or preventing your site from loading, better known as the "WordPress blank page error".

Testing a plugin update before installing it in a production environment isn´t done very often by most users because existing staging solutions are too complex and need a lot of time to create a
an up-to-date copy of your website.

You may be afraid of installing plugins updates because you follow the rule "never touch a running system" with having in mind that untested updates are increasing the risk of breaking your site.
This is one of the main reasons why WordPress installations are often outdated, not updated at all, and insecure because of this non-update behavior.

<strong>It's time to change this, so there is no easier way than using "WP STAGING" for cloning, backup, and migration of your WordPress website.</strong>

= How to install and set up a staging site / site backup? =
Install WP STAGING backup via the admin dashboard. Go to  'Plugins', click 'Add New' and search the plugins for 'WP STAGING'. Install the plugin with 'Install Now'.
After installation, go to WP STAGING > Staging Sites and create your first staging / backup site

= Is WP STAGING a backup plugin? =
Even though you can use WP STAGING for backup purposes, the free version is not a usual backup plugin per general definition. WP STAGING creates a clone of your entire website which you can immediately use for developing and testing.
You can even use it as some kind of backup in case something happens to your production site but only the WP STAGING | PRO pro version allows you to download the backup to your local computer. There are many other popular backup plugins out there but our goal is to bring the reliability and performance of a backup plugin to a new level. So instead of offering our backup feature free of charge, we think it's time to provide a full-fledged premium backup solution with enterprise code quality affordable for everyone.

[Video: How we run automated tests for WP STAGING](https://www.youtube.com/watch?v=Tf9C9Pgu7Bs)

= What is the difference between WP STAGING backup and other backup plugins? =

----------------------------------------------
Note: WP STAGING | PRO provides more advanced backup functionality which can be compared with the below-mentioned backup plugins. The speed and performance of WP STAGING's backup feature often exceed even the biggest and well-established backup plugins.
We are now adding more and more advanced backup features to deliver what other existing backup plugins are still missing.
----------------------------------------------

You may have heard about other popular backup plugins like All in one Migration, BackWPUp, BackupWordPress, Simple Backup, WordPress Backup to Dropbox, or similar WordPress backup plugins and now wonder about the difference between WP STAGING and those backup tools.
Other backup plugins usually create a backup of your WordPress filesystem and a database backup which you can use to restore your website in case it became corrupted or you want to go back in time to a previous state.
The backup files are compressed and can not be executed directly. WP STAGING on the other hand creates a full backup of the whole file system and the database in a working state that you can open like your original production website.

Even though WP STAGING basic version comes with some backup capabilities its main purpose is to create a clone of your website which you can work on. It harmonies very well with all the mentioned backup plugins above and we recommend that you use it in conjunction with these backup plugins.

Note, that some free backup plugins are not able to support custom tables. (For instance the free version of Updraft plus backup plugin). In that case, your backup plugin is not able to create a backup of your staging site when it is executed on the production site.
The reason is that the tables created by WP STAGING are custom tables beginning with another table prefix.
To bypass this limitation and to be able to create a backup of your staging site, you can set up your backup plugin on the staging site and create the backup from that location. This works well with every available WordPress backup plugin.

= I want to backup my local website and copy it to production and another host =
If you want to migrate your local website to an already existing production site you can use our pro version WP STAGING | PRO or a tool like WP Migrate DB which copies only the database.
WP STAGING is intended for creating a staging site with the latest data from your production site or creating a backup of it.

= What are the benefits compared to a plugin like Duplicator? =
We like the Duplicator plugin. Even if it is not as fast as WP STAGING for creating a backup it is a great tool for migrating from a development site to production one or from production site to development one and a good tool to create a backup of your WordPress website.
The downside is that before you can even create an export or backup file with Duplicator a lot of adjustments, manual interventions, and requirements are needed before you can start the backup process.
The backup plugin Duplicator also needs some skills to be able to create a backup and development/staging site, where WP STAGING does not need more than a click from you to create a backup or staging site.
Duplicator is best placed to be a tool for the first-time creation of your production site. This is something where it is very handy and powerful.

If you have created a local or web-hosted development site and you need to migrate this site the first time to your production domain then you are doing nothing wrong with using
the Duplicator plugin! If you need all your latest production data like posts, updated plugins, theme data, and styles in a testing environment or want to create a quick backup before testing out something then we recommend using WP STAGING instead!

If speed, performance, and code quality are a matter for you as well, give WP STAGING | PRO a try.

= I can not log in to the staging / backup site =
If you are using a security plugin like Wordfence, iThemes Security, All In One WP Security & Firewall, or a plugin that hides the WordPress default login URL make sure that you have installed the latest version of WP STAGING to access your cloned backup site.
If you can still not log in to your staging / backup site, you can go to WP STAGING > settings and disable there the WP STAGING extra authentication. Your admin dashboard will still be protected and not accessible to public users.

= Can I activate permalinks on the staging site? =

Permalinks are disabled on the staging / backup site after first time cloning / backup creation
[Read here](https://wp-staging.com/docs/activate-permalinks-staging-site/ "activate permalinks on staging site") how to activate permalinks on the staging site.

= How to Transfer WordPress to another Host or Domain by Using a WP STAGING Backup
The pro version of WP STAGING can backup your whole WordPress website. (In the future we are implementing a basic free version of our sophisticated backup feature into this free version as well)
With this backup function, you can backup and copy your entire WordPress website to another domain, new host, or new server very easily, and often faster and more reliable than with any other existing backup plugins.
Have a look at [https://wp-staging.com/docs/how-to-migrate-your-wordpress-site-to-a-new-host/](this article) which gives a good introduction into the backup feature.

= Can I give you some feedback? =
This plugin has been created in thousands of hours and works even with the smallest shared web hosting package.
We also use enterprise-level approved testing coding environment to make sure that the cloning and backup process run rock-solid on your system.
If you are a developer you will probably like to hear that we use Codeception and PHPUnit for our backup software.

As there are infinite variations of possible server constellations it still can happen that something does not work for you 100%. In that case,
please open a [support request](https://wp-staging.com/support/ "support request") and describe your issue.

== Official Site ==
https://wp-staging.com

== Installation ==
1. Download the backup & clone plugin "wp-staging-pro.zip" from your account or the mail we've sent to you.
2. Upload and install it via the WordPress plugin backend wp-admin > plugins > add new > uploads
3. Activate the plugin through the 'Plugins' menu in WordPress.
4. Go to: Start Plugins->Staging

== Official Site ==
https://wp-staging.com

== Installation ==
1. Download the file "wp-staging-pro.zip":
2. Upload and install it via the WordPress plugin backend wp-admin > plugins > add new > uploads
3. Activate the plugin through the 'Plugins' menu in WordPress.
4. Start Plugins->Staging

== Screenshots ==

1. Create new WordPress staging / backup site
2. Select name for staging / backup site
3. Select folders to include in staging / backup site
4. Cloning / backup processing
5. Listed staging / backup sites
5. Listed staging / backup sites
6. Open, edit & delete staging / backup sites
7. Login to staging / backup site
4. Demo of a staging / backup site

== Changelog ==

= 4.1.6 =
* New: Support for WordPress 5.9
* New: Add filter to change the cache folder for creating & restoring backups #1528
* New: Huge performance improvement for search & replace in cloning / pushing / backup process #1522
* Fix: Call to undefined function human_readable_duration() on backup creation if WP is older than 5.1 #1527 #1525 #1535
* Dev: Add unit tests for Times class
* Dev: Update db_version in SQL dumps to match WordPress 5.9 db version #1539
* Dev: Add command to get db_version from database

= 4.1.5 =
* New: Add support for WordPress 5.8.3
* New: Add filter for excluding files during cloning / backup #1494
* New: Add filter for overwriting max execution for database backup restore #1512
* New: Add filter to allow overwriting of the maximum allowed request time to make sure backup restore works for huge files. (19.000.000M database rows) #1510
* Fix: If cloning a multisite subsite into external database, it does not clone / backup wp_users and wp_usermeta #1515
* Fix: Skip tmp single file plugin during backup PUSH copy process #1491
* Fix: Preserve table selection during PUSH and UPDATE even when all backup tables unselected #1488
* Fix: Make sure maximum memory consumption during cloning or backup is never higher than 256MB #1502
* Fix: Use custom implementation of wp_timezone() for backward compatibility to WordPress older than 5.3 #1505
* Fix: Override FileObject::fgets to make them behave exactly from SplFileObject of PHP < 8.0.1 #1506
* Tweak: Show custom uploads folder in tooltip description and explain better that changing a symlink image will also change the image on the production site. #1495

= 4.1.4 =
* Hotfix: Fix CLONE PUSH BACKUP on Medium and High CPU Load on WP STAGING PRO 4.1.3. Improve Performance of database backup #1492

= 4.1.3 =
* New: If cpu load setting is low make use of the file copy limit for pushing process to increase copy speed #1485
* Enh: Add warning notice if WP_CRON_DISABLED is set to true as BG Processing depends upon it #1467
* Fix: Fix email sending tooltip wording #1469
* Fix: Make FileObject::seek behave exactly as SplFileObject::seek from PHP < 8.0 #1480
* Fix: Search Replace now works for Visual Composer / WP Bakery encoded pages #1442
* Fix: Adjust CSS of the "Backup in Progress" element #1466
* Fix: Clarify email sending tooltip description #1469
* Fix: Adjust CSS of the loader icon #1487
* Dev: Cancel pending or running github actions fast tests if there is a new push on the same PR #1486

= 4.1.2 =
* New: Create fully customizable scheduled backup plans that will constantly create backups of your site. Including backup rotation to manage disk space. #1219 #1461
* Fix: Add own implementation of get_current_network_id() for backward compatibility #1438
* Fix: Updating or resetting staging / backup site skips all WordPress core folders #1435
* Fix: Prevent 504 Gateway Timeout issue during Backup restore by force a low CPU LOAD (i.e. 10s) #1420
* Fix: Wrong directory path is displayed when update/reset a staging / backup site #1447
* Fix: Override SplFileObject::seek to make it consistent across all PHP version including PHP 8 #1444
* Tweak: Retain WP STAGING ( backup ) options during push #1417
* Tweak: Make PHP 5.6 minimum supported PHP version #1448
* Tweak: Set WordPress 4.4 as minimum required WordPress version #1449
* Dev: Fix Queue working in PHP 8 and Add PHP 8 unit tests in fast tests #1450

= 4.1.1 =
* Fix: Update notice is shown even when using latest version #1398
* Fix: Backup & cloning 100% compatible with PHP 8.0.12 #1281
* Fix: Skip search replace on backup & cloning query if it's size exceed preg functions limit #1404
* Fix: Skip inserting backup & cloning query if it exceeds mysql max_allowed_package. Show warning to user #1405
* Fix: Make db option wpstg_staging_sites always return an array #1413
* Fix: Fix dependency injection for backup notices class. Solve conflict with TranslatePress #1416
* Tweak: Use php version number as tag for php docker container #1407
* Tweak: Improve symlink tooltip text #1411
* Tweak: Refactor WP STAGING Pro to WP STAGING | PRO in notices #1409
* Tweak: Remove 16 characters limitation for the backup & CLONE NAME and keep it for CLONE DIRECTORY #1412

= 4.1.0 =
* New: Show a summary of selected tables and plugins in the push selection
* Fix: WP Staging's variable not declared on plugins page #1373
* Fix: Fix PUSH process when no table is selected #1387
* Fix: Enclose table name with backticks during quering in CLONE and PUSH jobs #1388

= 4.0.9 =
* New: Ask user to allow sending non-personalized usage information for improving the user experience
* Fix: Feedback modal not opened after disabling a plugin #1373
* Fix: Prevent cloning error by enclosing table name with backticks in CLONE and PUSH jobs #1388
* Tweak: Disable the notice about not messing with uploads or wp-content directories #1385

= 4.0.8 =
* Fix: Push from staging site in external database can fail if there are many tables to replace due to exceeding maximum allowed number of external database connections #1379
* Enh: Added buttons to ease process of selecting tables during PUSH process. #1377

= 4.0.7 =
* New: Add filters to filter rows during cloning and pushing process #1369
* Tweak: Make staging site upgrade routine run always #1358
* Fix: Fix issue about checking rest url #1354
* Fix: Fix exclude condition for tables during PUSH #1364
* Fix: Fix search replace issue with table having composite primary key #1371
* Dev: Add custom debug logger for WP STAGING #1372

= 4.0.6 =
* New: Better support for custom tables without a wp core table prefix. Allow cloning/backup of custom tables that do not begin with a WP table prefix to an external database #1304
* New: Now you can create a staging environment for your entire multisite network #1263
* New: Add new logic for showing update notification for PRO version, compatible for staged rollout releases #1308
* New: Show warning notice about not changing wp-content or uploads dir path on staging site #1313
* Tweak: Lower memory consumption on backup creation #1301
* Tweak: Fix open staging site button and text #1321
* Tweak: Layout of database comparison modal #1332
* Tweak: Simplify last step of the push and make it faster and more robust #1351
* Tweak: Collect more information to system info #1356
* Fix: Duplicate primary key error that could occur on Push #1322
* Fix: Dont rename themes/plugins with tmp prefix during push if content cleaning option is enabled #1305
* Fix: No search & replace of wp option db_version if table prefix is db_, during CLONE/PUSH #1307
* Fix: Avoid upgrade error if wp option wpstg_staging_sites is empty or not an array not an array #1331
* Fix: Show an error if table can not be copied for any reason on cloning / backup process #1302
* Fix: Vertical center align finish label after push #1311
* Fix: Use WordPress local timezone when logging for clone and backups #1323
* Fix: Better support for custom plugin directories on the staging site #1314
* Fix: Not all cloning/backup settings are cleaned during uninstall #1325
* Fix: Staging/backup site does not have orange admin bar after cloning #1329
* Fix: Warning if array key offset does not exist on search & replace #1334
* Fix: Disable WordFence plugin on the staging site to prevent by renaming .user.ini to .user.ini.bak #1338
* Fix: Prevent empty database prefix in staging site options if site is cloned to external database #1340
* Fix: List of staging sites contains duplicate entries if staging sites were initially created with wp staging free 2.8.6, then upgraded to pro 4.0.3 and pro 4.0.5 #1349
* Fix: Show error and stop cloning / backup process if unable to create staging / backup site destination folder #1348

= 4.0.5 =
* New: We introduced opt-in Analytics to guide further development of WP STAGING #1122
* Fix: New pro version does not recognize staging sites created with older free version #1293
#1306
#1366

= 4.0.4 =
* Enh: Refactor the wp_login action hook to work with different parameter count than the one in WordPress Core #1223
* Enh: Sort new staging sites in descending order by creation time #1226
* Enh: Warn if creating a backup in PHP 32 bit version #1231
* Enh: Update the backup upload success message #1221
* Enh: Show a notice if there is a new WP STAGING free version #1250
* Enh: Rename db option wpstg_existing_clones_beta to wpstg_staging_sites #1211
* Enh: Update the warning message shown when the delete process of the staging site fails #1257
* Enh: Allow use of REST API on staging sites without login #1287
* Enh: Add new EDD software licensing updater #1294
* Fix: Fix a rare issue that could happen when creating a new staging site or restoring a backup when there is a row with primary key with value zero #1271
* Fix: Try to repair MyISAM table if it's corrupt when creating a Backup #1222
* Fix: Fix an issue on backup creation that would cause a database export to loop when encountering a table with negative integers or zeros as a primary key value #1251
* Fix: Lock specific tables while exporting a backup, to prevent a rare duplicated row issue #1245
* Fix: If the memory exhausts during a database export using the Backup feature, lower the memory usage/speed of the export and try again automatically #1230
* Fix: Prevent failure of adding database to backup from causing a loop #1231
* Fix: Fix issue when old clones from version 1.1.6 or lower replaces the existing clones from later version when upgrading from FREE to PRO version #1233
* Fix: Fix inconsistent Unselect All Tables button's action #1243
* Fix: Replace undefined date with proper formatted date during backups for some warning and critical messages #1244
* Fix: Split file scanning of wp-content into scanning of plugins, themes, uploads and other directories to reduce timeout issues #1247
* Fix: Rename .user.ini to .user.ini.bak after cloning to reduce fatal errors on staging site. Also show a notice. #1255
* Fix: Skip scanning the root directory if all other directories are unselected #1256
* Fix: Show correct insufficient space message instead of permission error if unable to copy due to insufficient space #1283
* Fix: Fix showing of error when unable to count tables rows and wrap table name when fetching rows during backup #1285
* Fix: Remove custom error handler that could cause errors due to notices being thrown #1292
* Fix: Fix an error that would occur when PHP lacked permission to get the size of a directory when pushing a staging site to production #1295
* Dev: Set the version of Selenium containers to 3.141.59-20210713 to avoid issues with broken latest version of selenium #1234

= 4.0.3 =
* New: Support for WordPress 5.8
* New: Show notice if uploads dir is outside WP Root #1138
* Enh: Show warning during restore if Backup was created on a server with PHP ini "short_open_tags", and restoring on a server with it disabled #1129
* Enh: Also show disabled permalink message in disabled items notice on the staging site and show a page builder (DIVI, Elementor etc) not working help link in wpstg page footer #1150
* Enh: Allow filtering the Backup directory using the `wpstg.backup.directory` filter #1167
* Enh: Decouple clone name and clone ID for better usage #1158
* Enh: Allow backups on the staging site #1172
* Enh: Show issue notice if backups is created on version >= 4.0.2 #1198
* Enh: Remove deprecated hooks call #1209
* Fix: Fix staging site when site has freemius script #1112
* Fix: Prefix 'wpstg' to sweetalerts Swal to avoid conflict with its other versions #1125
* Fix: Fix a bug in the backup export logic that would loop when encountering a file with non-empty contents that PHP would evaluate as false #1126
* Fix: Set default values for wpstg settings on plugin activate event if wpstg settings not already set #1135
* Fix: Fix the problem when unable to access the staging site because production site have different siteurl or home url and either one of them is having www. prefix #1136
* Fix: Restore a backup with VIEWs or TABLEs with special MySQL configurations such as DEFINER #1139
* Fix: Fix issue where tab triangle was inconsistent by using css based tab triangle #1148
* Fix: Fix issue where backup tmp folder cleaning process closes logs modal #1157
* Fix: Reduce time to query INFORMATION_SCHEMA on some shared hosts from ~10s to one millisecond #1154
* Fix: Fix a bug on backup creation that would not prefix the table name if a MySQL View is selecting data from another MySQL View #1155
* Fix: Fix a bug on backup restore that would fail when trying to create a MySQL View that selects data from another MySQL View that has not been created yet due to order of creation #1155
* Fix: Check available free disk space on large disks on 32-bit PHP #1179
* Fix: Fix a bug where a PHP memory_limit of -1 (Unlimited) would be interpreted as 64MB, now it's interpreted as 512MB #1178
* Fix: Fix download of .wpstg files on Bitnami/AWS Lightsail servers #1181
* Fix: Remove usages of `abstract static` methods that would violate `strict` PHP checks #1185
* Fix: Cloning a site resets the settings to the default ones #1183
* Fix: Fix a bug on backup creation that would prevent user from logging in after restoring a backup on a site with a different WPDb prefix #1169
* Fix: Allow backup restore with a warning if file count is different than expected, improve backup file count logic #1189
* Fix: Fix Clone RESET and Clone DELETE when unable to delete file due to permission error #1196
* Fix: Fix an issue when canceling a push confirm redirects to empty page #1206
* Fix: Add missing back button and hide cancel button after clone UPDATE and clone RESET #1207
* Fix: Fix Error in JS console related to registering of main-menu in page where it was not available #1205
* Dev: Add wrapper methods for deprecated hooks functions to support WordPress < 4.6 #1209


= 4.0.2 =
* Enh: Replace css based hover with pure js hoverintent for tooltips #1106
* Enh: Cleanup logs older than 7 days automatically #1116
* Enh: Update the version to check in Outdated WP Staging Hooks notice #1118
* Fix: Fixed conflict with Rank Math Seo PRO when Rank Math Seo PRO is activated network wide in multisites #1111
* Fix: Make Scan::hasFreeDiskSpace() return other info even if disk_free_space is unavailable #1093
* Fix: Fix an issue where MySQL views were not being deleted correctly during the database cleanup step of the backup export #1098

= 4.0.1 =
* Enh: Delete Optimizer Plugin on WP Staging plugins deactivate instead of uninstall #1096
* Enh: Optimize compatibility to restore backup generated in newer versions of MySQL #1109
* Enh: More robust backup upload by allowing a small size difference before considering it invalid #1110
* Fix: Replace the deprecated of calling a non-static method in daily version check hooks #1092
* Fix: Fixed an issue where a column with reserved MySQL keywords would cause a malformed MySQL query during the backup export #1097
* Fix: Try catch all instance of directory iterators #1101
* Dev: Refactor the JS code of the Backup feature for better readability and maintainability #1102

= 4.0.0 =
* New: You can now Backup, Download and Restore your entire website anytime you want! Easily migrate your website to another server by uploading the Backup and Restoring it on another server! #746
* Enh: Schedule the uploads backup to be deleted after one week if that option was selected during push #980
* Enh: Allow copying of only that symlink whose source is a directory #979
* Enh: Show notice only to user who can manage_options if wp_options table is missing primary key #1009
* Enh: Gracefully handle disk full during backup creation/restore #1041
* Fix: Handle error properly for Filesystem::delete() method #974
* Fix: Remove loading wpstg scripts as ESM to allow loading them as asynchronous #1007
* Fix: Properly handle exception while cleaning themes and plugins bak and tmp directories #1017
* Fix: Delete the clone even if in any case a corrupted delete job cache file existed for delete job #1033
* Fix: No cloning/pushing logs were written to file. Now fixed. #1040
* Fix: Wrap wp_doing_ajax in a adapter and use that adapter to call it to make it usable in WP < 4.7  #1047
* Fix: Fix typo and wrap up text in i18n for src/Backend/views/clone/ajax/start.php #1051
* Fix: Fix missing clone options warning during scanning process for old clones for UPDATE and RESET #1058
* Fix: Make isExcludedDirectories condition works for relative directories path too #1054
* Fix: Allow backup to be exported/restored from/to sites with custom WordPress directory structures #1032
* Fix: Set donation link to redirect to WP Staging pricing page #1080
* Dev: Add a shortcut to allow to use the DI container as a Service Locator easier under some circumstances #1039
* Dev: Add trait to allow for easier use of the `uopz` extension in tests #1053
* Dev: Replace const related tests logic with UOPZ for better readability and control #1079

= 3.2.6 =
* Feat: Compatible up to WordPress 5.7.2
* Enh: Preserve directories/tables selection and excludes rules for RESET and UPDATE process #809
* Enh: If any wpstg process is running allow to stop that process within RESET modal #942
* Enh: Properly show error message if unable to scan a directory (especially for Windows IIS env) #960
* Fix: Fix multisite subsite capabilities on the staging site #852
* Fix: Properly resets the properties between Cloning tasks #896
* Fix: Avoid PHP warning when building version of missing asset file #929
* Fix: Clean leftover wpstg-tmp-* and wpstg-bak-* directories from plugins and themes directories while push #954
* Fix: Make RESET modal show error message within modal on failed response instead of browser logs #942
* Fix: Replace wpstgGetCloneSettings() in mail-settings.php with CloneOption::get() #956
* Fix: Little typo changed effect to affect #963
* Fix: Made node_modules dir to be only excluded from WP Staging's Plugins #963
* Fix: Fix UPDATE and RESET for old wpstg clones which are without appended underscore db prefix #958
* Fix: Always use database table prefix in lowercase for Windows environment #967

= 3.2.5 =
* Feat: Compatible up to WordPress 5.7.1 #855
* Feat: Check disk space according to selected directories #761
* Feat: Make the staging site admin bar background color customizable #758
* Feat: Add UI to exclude certain files or folders by rules #726
* Feat: Show success popup on UPDATE and RESET jobs' completion #818
* Enh: Disallow invalid character in table prefix #819
* Enh: Directory navigation in file selection to infinite deep level #768
* Enh: Remove dependency on Symfony libraries #888
* Fix: Fix copy of big external tables #795
* Fix: Exclude mainsite uploads dir content while cloning non-main sites in multisite #755
* Fix: Fix RESET process for destination dir outside WP Root #808
* Fix: Exclude blog_versions table from SearchReplace job as well #807
* Fix: Fix issue where sub directories can only be collapsed when parent directory is checked #835
* Fix: Push now works if the ANSI_QUOTES sql_mode is enabled, as it is by default on Digital Ocean #839
* Fix: Fix isStagingSiteCloneable index not found when saving settings on old staging site #846
* Dev: Add Queue and Background Processing first layer of support #743
* Fix: Fix: Showing warning alert during PUSH when user tries to navigate to other page or close the browser/tab #848
* Fix: Fix unable to connect external database when making sure staging site doesn't use production site database and prefix #851
* Fix: Database backup's DELETE now works when deleting backup with no table #857
* Fix: Fix cloning not working due to warnIfClosingDuringProcess not being called properly #871
* Dev: Integrated Rollup for bundling/minifying/concatenating assets #828
* Dev: Remove console.log() output #874

WP STAGING Backup & Cloning | Full Changelog:
[https://wp-staging.com/wp-staging-pro-changelog/](https://wp-staging.com/wp-staging-pro-changelog/)


== Upgrade Notice ==

= 4.1.5 =
* New: Add support for WordPress 5.8.3
* New: Add filter for excluding files during cloning / backup #1494
* New: Add filter for overwriting max execution for database backup restore #1512
* New: Add filter to allow overwriting of the maximum allowed request time to make sure backup restore works for huge files. (19.000.000M database rows) #1510
* Fix: If cloning a multisite subsite into external database, it does not clone / backup wp_users and wp_usermeta #1515
* Fix: Skip tmp single file plugin during backup PUSH copy process #1491
* Fix: Preserve table selection during PUSH and UPDATE even when all backup tables unselected #1488
* Fix: Make sure maximum memory consumption during cloning or backup is never higher than 256MB #1502
* Fix: Use custom implementation of wp_timezone() for backward compatibility to WordPress older than 5.3 #1505
* Fix: Override FileObject::fgets to make them behave exactly from SplFileObject of PHP < 8.0.1 #1506
* Tweak: Show custom uploads folder in tooltip description and explain better that changing a symlink image will also change the image on the production site. #1495