<?php

/**
 * Plugin Name: Custom WooCommerce Email
 */

if (!defined('ABSPATH')) {
    return;
}


class Custom_WC_Email
{

    /**
     * Custom_WC_Email constructor.
     */
    public function __construct()
    {
        // Filtering the emails and adding our own email.
        add_filter('woocommerce_email_classes', array($this, 'register_email'), 90, 1);

        // Absolute path to the plugin folder.
        define('CUSTOM_WC_EMAIL_PATH', plugin_dir_path(__FILE__));
    }

    /**
     * @param array $emails
     *
     * @return array
     */
    public function register_email($emails)
    {
        require_once 'emails/class-wc-patient-detail.php';

        $emails['WC_Patient_Details'] = new WC_Patient_Details();

        return $emails;
    }
}

new Custom_WC_Email();
